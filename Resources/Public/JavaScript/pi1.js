$(document).ready(function(){	
	mooxMarketplaceInitClearButtons();
	mooxMarketplaceInitComplexify();
	mooxMarketplaceInitFileinput();
	mooxMarketplaceInitDatePicker();
	mooxMarketplaceInitTooltips();
	mooxMarketplaceInitDropdowns();
	mooxMarketplaceInitPayment();
	mooxMarketplaceInitTreeSelector();	
	mooxMarketplaceInitClientValidation();
	
	$('.tx-moox-marketplace form select[data-id="variant"], .tx-moox-marketplace form input[data-id="variant"]').on('change', function() {
		
		form = $(this).parents('form').first();
		
		name = $(this).attr("name");
		name = name.substr(0, name.indexOf("["));
		name = name+"[reload]";
		
		$('<input>').attr({
			type: 'hidden',
			name: 'client-validation',
			id: 'client-validation',
			value: 'disabled'
		}).appendTo(form);
		
		$('<input>').attr({
			type: 'hidden',
			name: name,
			value: '1'
		}).appendTo(form);
		
		form.submit();
	});
});