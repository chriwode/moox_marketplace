TYPO3.jQuery(document).ready(function() {	
	TYPO3.jQuery( "form[name='editform']" ).submit(function( event ) {
		if(TYPO3.jQuery("#moox-marketplace-tca-tree-selector-min")){
			var countOptions = TYPO3.jQuery('select.moox-marketplace-tca-tree-selector-display option').size();
			var minCount = TYPO3.jQuery("#moox-marketplace-tca-tree-selector-min").val();
			if(countOptions<minCount){
				alert("Sie müssem mindestens " + minCount + " Kategorie(n) auswählen.");
				event.preventDefault();
			}
		}
	});	
	//TYPO3.jQuery(".moox-marketplace-tca-tree-selector-wrapper.is-parent").bind("contextmenu",function(e){
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-wrapper").bind("contextmenu",function(e){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-marketplace-tca-tree-selector-wrapper-/g,'');
		TYPO3.jQuery(".moox-marketplace-tca-tree-selector-context").hide();
		TYPO3.jQuery("#moox-marketplace-tca-tree-selector-context-" + value).show();		
	   return false;
	});
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-context").mouseleave(function() {
		TYPO3.jQuery(this).hide();
	}); 	
});	

function mooxMarketplaceUpdateCategoriesField(id){
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	var cnt = 1;
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-wrapper .t3-icon-toolbar-menu-shortcut.moox-marketplace-tca-tree-selector-icon").addClass( "t3-icon-mimetypes t3-icon-mimetypes-x t3-icon-x-sys_category moox-marketplace-tca-tree-selector-icon" );
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-wrapper .t3-icon-toolbar-menu-shortcut.moox-marketplace-tca-tree-selector-icon").removeClass( "t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-marketplace-tca-tree-selector-icon" );	
	if(countOptions>0){
		TYPO3.jQuery(id + '_list option').each( function() {
			TYPO3.jQuery(this).text(TYPO3.jQuery(this).text().replace(/ \(Hauptkategorie\)/g,''));			
			if(cnt==1){
				TYPO3.jQuery("#moox-marketplace-tca-tree-selector-hidden").val(TYPO3.jQuery(this).attr('value'));
				TYPO3.jQuery(this).text(TYPO3.jQuery(this).text() + " (Hauptkategorie)");											
				TYPO3.jQuery("#moox-marketplace-tca-tree-selector-icon-" + TYPO3.jQuery(this).attr('value')).removeClass( "t3-icon-mimetypes t3-icon-mimetypes-x t3-icon-x-sys_category moox-marketplace-tca-tree-selector-icon" );
				TYPO3.jQuery("#moox-marketplace-tca-tree-selector-icon-" + TYPO3.jQuery(this).attr('value')).addClass( "t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-marketplace-tca-tree-selector-icon" );				
			} else {
				TYPO3.jQuery("#moox-marketplace-tca-tree-selector-hidden").val(TYPO3.jQuery("#moox-marketplace-tca-tree-selector-hidden").val() + "," + TYPO3.jQuery(this).attr('value'));
			}
			cnt = cnt + 1;
		});	
	} else {
		TYPO3.jQuery("#moox-marketplace-tca-tree-selector-hidden").val("");
	}
	TYPO3.jQuery("#moox-marketplace-tca-tree-selector-cnt").text(countOptions);
}

function mooxMarketplaceAddOption(id,value,label){	
	var alreadyExisting = false;
	var maxCount = TYPO3.jQuery("#moox-marketplace-tca-tree-selector-max").val();
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	if(countOptions<maxCount || maxCount==0){
		TYPO3.jQuery(id + '_list option').each(function(){
			if(TYPO3.jQuery(this).attr('value')==value){
				alreadyExisting = true;			
			}
		});
		TYPO3.jQuery(id + '_list option').removeAttr('selected');
		if(!alreadyExisting){
			TYPO3.jQuery(id + '_list').append( new Option(label,value,false,true) );
			TYPO3.jQuery("#moox-marketplace-tca-tree-selector-wrapper-" + value).addClass( "moox-marketplace-tca-tree-selector-active" );
		}
	}
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceToggleOption(id,value,label){	
	var alreadyExisting = false;
	var maxCount = TYPO3.jQuery("#moox-marketplace-tca-tree-selector-max").val();
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	TYPO3.jQuery(id + '_list option').each(function(){
		if(TYPO3.jQuery(this).attr('value')==value){
			alreadyExisting = true;			
		}
    });
	TYPO3.jQuery(id + '_list option').removeAttr('selected');
	if(!alreadyExisting){
		if(countOptions<maxCount || maxCount==0){
			TYPO3.jQuery(id + '_list').append( new Option(label,value,false,true) );
			TYPO3.jQuery("#moox-marketplace-tca-tree-selector-wrapper-" + value).addClass( "moox-marketplace-tca-tree-selector-active" );
		}
	} else {
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').remove();
		TYPO3.jQuery("#moox-marketplace-tca-tree-selector-wrapper-" + value).removeClass( "moox-marketplace-tca-tree-selector-active" );
	}
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceAddAllBranchOptions(id,value){			
	TYPO3.jQuery("li.moox-marketplace-tca-tree-selector-" + value).each(function(){
		branchValue = TYPO3.jQuery(this).attr('id').replace(/moox-marketplace-tca-tree-selector-/g,'');		
		mooxMarketplaceAddOption(id,branchValue,TYPO3.jQuery("#moox-marketplace-tca-tree-selector-title-" + branchValue).attr('title'));		
    });
	TYPO3.jQuery("li.moox-marketplace-tca-tree-selector-" + value).each(function(){
		branchValue = TYPO3.jQuery(this).attr('id').replace(/moox-marketplace-tca-tree-selector-/g,'');
		TYPO3.jQuery(id + '_list option[value="' + branchValue + '"]').attr('selected',true);		
    });
	mooxMarketplaceUpdateCategoriesField(id);
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-context").hide();
}
		
function mooxMarketplaceAddAllOptions(id){	
		
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-wrapper").each(function(){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-marketplace-tca-tree-selector-wrapper-/g,'');
		mooxMarketplaceAddOption(id,value,TYPO3.jQuery("#moox-marketplace-tca-tree-selector-title-" + value).attr('title'));		
    });
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-wrapper").each(function(){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-marketplace-tca-tree-selector-wrapper-/g,'');
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').attr('selected',true);		
    });	
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceRemoveOptions(id){	
	TYPO3.jQuery(id + '_list option:selected').each(function(){
		TYPO3.jQuery("#moox-marketplace-tca-tree-selector-wrapper-" + TYPO3.jQuery(this).attr('value')).removeClass( "moox-marketplace-tca-tree-selector-active" );
		TYPO3.jQuery(this).remove();
    });
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceRemoveAllOptions(id){	
	TYPO3.jQuery(id + '_list option').each(function(){
		TYPO3.jQuery("#moox-marketplace-tca-tree-selector-wrapper-" + TYPO3.jQuery(this).attr('value')).removeClass( "moox-marketplace-tca-tree-selector-active" );
		TYPO3.jQuery(this).remove();
    });
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceRemoveAllBranchOptions(id,value){	
	TYPO3.jQuery("li.moox-marketplace-tca-tree-selector-" + value).each(function(){
		value = TYPO3.jQuery(this).attr('id').replace(/moox-marketplace-tca-tree-selector-/g,'');
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').each(function(){
			TYPO3.jQuery("div.moox-marketplace-tca-tree-selector-" + TYPO3.jQuery(this).attr('value')).removeClass( "moox-marketplace-tca-tree-selector-active" );
			TYPO3.jQuery(this).remove();
		});	
    });		
	mooxMarketplaceUpdateCategoriesField(id);
	TYPO3.jQuery(".moox-marketplace-tca-tree-selector-context").hide();
}

function mooxMarketplaceSetMainCategory(id,value,label){	
	
	value = typeof value !== 'undefined' ? value : 0;
	label = typeof label !== 'undefined' ? label : '';
	
	var newPos = 0;
	var countOptions = TYPO3.jQuery(id + '_list option:selected').size();
	if(value>0){
		mooxMarketplaceAddOption(id,value,label);
		TYPO3.jQuery(id + '_list option[value="' + value + '"]').remove();
		TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+value+"' selected='selected'>"+ label +"</option>");		
		TYPO3.jQuery(".moox-marketplace-tca-tree-selector-context").hide();
	} else {
		if(countOptions>1){
			alert("Bitte wählen Sie nur eine Kategorie als Hauptkategorie aus");
		} else {
			TYPO3.jQuery(id + '_list option:selected').each( function() {		
				value = TYPO3.jQuery(this).val();
				TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+value+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
				TYPO3.jQuery(this).remove();		
				newPos = newPos + 1;
			});					
		}
	}
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceMoveOptionsToTop(id){	
	var newPos = 0;
	TYPO3.jQuery(id + '_list option:selected').each( function() {		
		TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
		TYPO3.jQuery(this).remove();		
		newPos = newPos + 1;
    });
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceMoveOptionsOneUp(id){	
	TYPO3.jQuery(id + '_list option:selected').each( function() {
		var newPos = TYPO3.jQuery(id + '_list option').index(this) - 1;
		if (newPos > -1) {
			TYPO3.jQuery(id + '_list option').eq(newPos).before("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
			TYPO3.jQuery(this).remove();
		}	
    });
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceMoveOptionsOneDown(id){	
	var countOptions = TYPO3.jQuery(id + '_list option').size();
	TYPO3.jQuery(TYPO3.jQuery(id + '_list option:selected').get().reverse()).each( function() {
		var newPos = TYPO3.jQuery(id + '_list option').index(this) + 1;
		if (newPos < countOptions) {
			TYPO3.jQuery(id + '_list option').eq(newPos).after("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
			TYPO3.jQuery(this).remove();
		}	
    });
	mooxMarketplaceUpdateCategoriesField(id);
}

function mooxMarketplaceMoveOptionsToBottom(id){	
	var newPos = TYPO3.jQuery(id + '_list option').size();
	newPos = newPos - 1;
	TYPO3.jQuery(TYPO3.jQuery(id + '_list option:selected').get().reverse()).each( function() {
		TYPO3.jQuery(id + '_list option').eq(newPos).after("<option value='"+TYPO3.jQuery(this).val()+"' selected='selected'>"+TYPO3.jQuery(this).text()+"</option>");
		TYPO3.jQuery(this).remove();
		newPos = newPos - 1;	
    });
	mooxMarketplaceUpdateCategoriesField(id);
}