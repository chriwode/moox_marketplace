<?php
namespace DCNGmbH\MooxMarketplace\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ClassLoader
 */
class ClassLoader implements \TYPO3\CMS\Core\SingletonInterface
{

    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend
     */
    protected $cacheInstance;

    /**
     * Register instance of this class as spl autoloader
     *
     * @return void
     */
    public static function registerAutoloader()
    {
        spl_autoload_register([new self(), 'loadClass'], true, true);
    }

    /**
     * Initialize cache
     *
     * @return \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend
     */
    public function initializeCache()
    {
        if (is_null($this->cacheInstance)) {
            /** @var CacheManager $cacheManager */
            $cacheManager = GeneralUtility::makeInstance(CacheManager::class);
            $this->cacheInstance = $cacheManager->getCache('moox_marketplace');
        }
        return $this->cacheInstance;
    }

    /**
     * Loads php files containing classes or interfaces part of the
     * classes directory of an extension.
     *
     * @param string $className Name of the class/interface to load
     * @return bool
     */
    public function loadClass($className)
    {
        $className = ltrim($className, '\\');
		
        if (!$this->isValidClassName($className)) {
            return false;
        }

        $cacheEntryIdentifier = 'tx_mooxmarketplace_' . strtolower(str_replace('/', '_', $this->changeClassName($className)));

        $classCache = $this->initializeCache();
        if (!empty($cacheEntryIdentifier) && !$classCache->has($cacheEntryIdentifier)) {
            require_once(ExtensionManagementUtility::extPath('moox_marketplace') . 'Classes/Utility/ClassCacheManager.php');

            /** @var ClassCacheManager $classCacheManager */
            $classCacheManager = GeneralUtility::makeInstance(ClassCacheManager::class);
            $classCacheManager->reBuild();
        }

        if (!empty($cacheEntryIdentifier) && $classCache->has($cacheEntryIdentifier)) {
            $classCache->requireOnce($cacheEntryIdentifier);
        }

        return true;
    }

    /**
     * Get extension key from namespaced classname
     *
     * @param string $className
     * @return string
     */
    protected function getExtensionKey($className)
    {
        $extensionKey = null;

        if (strpos($className, '\\') !== false) {
            $namespaceParts = GeneralUtility::trimExplode('\\', $className, 0,
                (substr($className, 0, 9) === 'TYPO3\\CMS' ? 4 : 3));
            array_pop($namespaceParts);
            $extensionKey = GeneralUtility::camelCaseToLowerCaseUnderscored(array_pop($namespaceParts));
        }

        return $extensionKey;
    }

    /**
     * Find out if a class name is valid
     *
     * @param string $className
     * @return bool
     */
    protected function isValidClassName($className)
    {
        if (GeneralUtility::isFirstPartOfStr($className, 'DCNGmbH\\MooxMarketplace\\')) {
            $modifiedClassName = $this->changeClassName($className);
            if (isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['moox_marketplace']['classes'][$modifiedClassName])) {
                return true;
            }
        }
        return false;
    }

    protected function changeClassName($className)
    {
        return str_replace('\\', '/', str_replace('DCNGmbH\\MooxMarketplace\\', '', $className));
    }
}