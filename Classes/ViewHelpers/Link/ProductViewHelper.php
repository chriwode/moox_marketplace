<?php
namespace DCNGmbH\MooxShop\ViewHelpers\Link;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package moox_shop
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */

/**
 * ViewHelper to render links from product records to detail view or page
 *
 * # Example: Basic link
 * <code>
 * <shop:link.product product="{product}" settings="{settings}">
 * 	{product.title}
 * </shop:link.product>
 * </code>
 * <output>
 * A link to the given product record using the product title as link text
 * </output>
 *
 * # Example: Set an additional attribute
 * # Description: Available: class, dir, id, lang, style, title, accesskey, tabindex, onclick
 * <code>
 * <shop:link.product product="{product}" settings="{settings}" class="a-link-class">fo</shop:link.product>
 * </code>
 * <output>
 * <a href="link" class="a-link-class">fo</a>
 * </output>
 *
 * # Example: Return the link only
 * <code>
 * <shop:link product="{product}" settings="{settings}" uriOnly="1" />
 * </code>
 * <output>
 * The uri is returned
 * </output>
 *
 */
class ProductViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Link\PageViewHelper {
	
	/** @var $cObj \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer */
	protected $cObj;	

	/**
	 * Render link to product
	 *
	 * @param \DCNGmbH\MooxShop\Domain\Model\Product $product current product object
	 * @param array $settings
	 * @param boolean $uriOnly return only the url without the a-tag
	 * @param array $configuration optional typolink configuration
	 * @return string link
	 */
	public function render(\DCNGmbH\MooxShop\Domain\Model\Product $product, array $settings = array(), $uriOnly = FALSE, $configuration = array()) {
		
		$this->init();

		$configuration = $this->getLinkToProduct($product, $tsSettings, $configuration);

		$url = $this->cObj->typoLink_URL($configuration);
		if ($uriOnly) {
			return $url;
		}

		$this->tag->addAttribute('href', $url);
		$this->tag->setContent($this->renderChildren());
		return $this->tag->render();
	}

	/**
	 * Generate the link configuration for the link to the product
	 *
	 * @param\DCNGmbH\MooxShop\Domain\Model\Product $product
	 * @param array $tsSettings
	 * @param array $configuration
	 * @return array
	 */
	protected function getLinkToProduct(\DCNGmbH\MooxShop\Domain\Model\Product $product, $tsSettings, array $configuration = array()) {

		$configuration['parameter'] 		= 18;
		$configuration['useCacheHash']		= 1;		
		$configuration['additionalParams'] .= '&tx_mooxshop_pi1[controller]=Product';
		$configuration['additionalParams'] .= '&tx_mooxshop_pi1[action]=detail';
		$configuration['additionalParams'] .= '&tx_mooxshop_pi1[product]=' . $product->getUid();
		
		return $configuration;
	}		

	/**
	 * Initialize properties
	 *
	 * @return void
	 */
	protected function init() {
		$this->cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
	}
}
