<?php
namespace DCNGmbH\MooxMarketplace\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DCNGmbH\MooxMarketplace\Domain\Repository\MooxRepository;
 
/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CategoryRepository extends MooxRepository {
	
	protected $defaultOrderings = array ('tstamp' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING);
	
	/**
	 * Returns a constraint array created by a given filter array
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface &$query
	 * @param \array $filter
	 * @param \array $constraints	
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
	 */
	protected function createFilterConstraints(\TYPO3\CMS\Extbase\Persistence\QueryInterface &$query,$filter = NULL,$constraints = NULL){
				
		if(is_null($constraints)){
			
			$constraints = array();
			
		}
		
		//restrict to moox_marketplace categories
		$constraints[] = $query->equals('variant', 'moox_marketplace');
		
		// bugfix to fix ignoring of storage page
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$pids = $query->getQuerySettings()->getStoragePageIds();
		if(count($pids)){
			$constraints[] = $query->in('pid', $pids);
		}
		
		if(isset($filter['deleted']) && is_numeric($filter['deleted'])){
			
			$constraints[] = $query->equals('deleted', $filter['deleted']);
			
		}
		
		if(isset($filter['parentCategory']) && $filter['parentCategory']>0){
			
			$constraints[] = $query->equals('parentCategory', $filter['feUser']);
			
		}		

		if(isset($filter['parentCategories']) && is_array($filter['parentCategories']) && count($filter['parentCategories'])){
			
			$constraints[] = $query->in('parentCategory', $filter['parentCategories']);
			
		}	
		
		if(count($constraints)<1){
			
			$constraints = NULL;
			
		}
		
		return $constraints;
	}	
	
	/**
	 * Override default findByUid function to enable also the option to turn of
	 * the enableField setting
	 *
	 * @param \integer $uid id of record
	 * @param \boolean $respectEnableFields if set to false, hidden records are shown
	 * @return \DCNGmbH\MooxMarketplace\Domain\Model\Category
	 */
	public function findByUid($uid, $respectEnableFields = TRUE) {
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

		return $query->matching(
			$query->logicalAnd(
				$query->equals('uid', $uid),
				$query->equals('deleted', 0)
			))->execute()->getFirst();
	}
		
	/**
	 * Find categories by a given storage pid and variant
	 *
	 * @param \integer $storagePage storage page
	 * @param \array $variants variants
	 * @param \integer $sysLanguageUid sys language uid
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
	 */
	public function findByVariantsAndStoragePage($storagePage = NULL, $variants = array(),$sysLanguageUid = 0) {
		
		$query = $this->createQuery();
		
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		
		if($sysLanguageUid>0){
			$query->getQuerySettings()->setSysLanguageUid($sysLanguageUid);			
		} 
		if($sysLanguageUid=="" || $sysLanguageUid<0){
			$sysLanguageUid = 0;
		}
		$langStmt = "sys_language_uid=".$sysLanguageUid." AND ";
		
		if(!is_array($variants)){
			$variants = array($variants);
		} 
		
		if(!is_null($storagePage)){
			$pidStmt ='pid='.(int)$storagePage.' AND ';
		}
		
		$variantStmt = "";
		foreach($variants as $variant){			
			$variantStmt .= ' OR FIND_IN_SET ("'.$variant.'", classified_variants)';
		}
		
		$stmt = 'SELECT * FROM sys_category WHERE variant="moox_marketplace" AND '.$langStmt.$pidStmt.'(classified_variants=""'.$variantStmt.') ORDER BY parent';
		
		return $query->statement($stmt)->execute();		
	}
}
