<?php
namespace DCNGmbH\MooxMarketplace\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ClassifiedRepository extends MooxRepository
{
    protected $defaultOrderings = ['tstamp' => QueryInterface::ORDER_DESCENDING];
    
    /**
     * Returns a constraint array created by a given filter array
     *
     * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @param array $filter
     * @param array $constraints
     * @return array
     */
    protected function createFilterConstraints(QueryInterface $query, $filter = null, $constraints = null)
    {
        if (is_null($constraints)) {
            $constraints = [];
        }
        
        if (isset($filter['feUser']) && is_object($filter['feUser'])) {
            $constraints[] = $query->equals('feUser', $filter['feUser']);
        }
        
        if (isset($filter['paid'])) {
            if ($filter['paid']) {
                $constraints[] = $query->greaterThan('paid', 0);
            } else {
                $constraints[] = $query->equals('paid', 0);
            }
        }

        if (isset($filter['categories']) && is_array($filter['categories']) && count($filter['categories'])) {
            $categoryConstraints = array();
            foreach ($filter['categories'] as $category) {
                $categoryConstraints[] = $query->contains('categories', $category);
            }
            $constraints[] = $query->logicalOr($categoryConstraints);
        }
        
        if (isset($filter['variant']) && is_string($filter['variant']) && $filter['variant']!='') {
            $constraints[] = $query->equals('variant', $filter['variant']);
        }
        
        if (isset($filter['variants']) && is_array($filter['variants']) && count($filter['variants'])) {
            $constraints[] = $query->in('variant', $filter['variants']);
        } elseif (isset($filter['variants']) && is_string($filter['variants'])) {
            $constraints[] = $query->in('variant', GeneralUtility::trimExplode(',', $filter['variants'], true));
        }
        
        if (count($constraints) < 1) {
            $constraints = null;
        }
        
        return $constraints;
    }

    /**
     * Override default findByUid function to enable also the option to turn of
     * the enableField setting
     *
     * @param int $uid id of record
     * @param bool $respectEnableFields if set to false, hidden records are shown
     * @return \DCNGmbH\MooxMarketplace\Domain\Model\Classified|object
     */
    public function findByUid($uid, $respectEnableFields = true)
    {
        $query = $this->createQuery();
        
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(!$respectEnableFields);

        $constrain = $query->logicalAnd(
            $query->equals('uid', $uid),
            $query->equals('deleted', 0)
        );

        return $query
            ->matching($constrain)
            ->execute()
            ->getFirst();
    }

    /**
     * find extended filter items
     *
     * @param array $field
     * @param array $storagePages
     * @param string $table
     * @param string $where
     * @return array
     */
    public function findTcaFieldValues($field, $storagePages = null, $table, $where = '')
    {
        $result = [];

        if ($table != '') {
            if (!is_array($storagePages)) {
                $storagePages = [$storagePages];
            }
            
            $query = $this->createQuery();
                    
            if (is_array($storagePages) && count($storagePages)) {
                $query->getQuerySettings()->setRespectStoragePage(false);
                $storagePageIdsStmt = ' AND pid IN (' . implode(',', $storagePages) . ')';
            }
            $query->getQuerySettings()->setRespectSysLanguage(true);
            $query->getQuerySettings()->setIgnoreEnableFields(false);
            $query->statement('SELECT uid,title FROM `' . $table . '` WHERE 1=1' . $storagePageIdsStmt . ' ' . $where);
            $resultDb = $query->execute(true);
                        
            foreach ($resultDb as $item) {
                $result[] = ['uid' => $item['uid'], 'title' => $item['title']];
            }
        }
        
        return $result;
    }
}
