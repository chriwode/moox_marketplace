<?php
namespace DCNGmbH\MooxMarketplace\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
 
/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Classified extends AbstractEntity {
	
	/**
	 * uid
	 *
	 * @var \integer
	 */
    protected $uid;
	
	/**
	 * pid
	 *
	 * @var \integer
	 */
    protected $pid;
	
	/**
	 * tstamp
	 *
	 * @var \integer
	 */
    protected $tstamp;		
	
	/**
	 * starttime
	 *
	 * @var \integer
	 */
    protected $starttime;
	
	/**
	 * endtime
	 *
	 * @var \integer
	 */
    protected $endtime;
	
	/**
	 * crdate
	 *
	 * @var \integer
	 */
    protected $crdate;
	
	/**
	 * hidden
	 *
	 * @var \integer
	 */
    protected $hidden;
	
	/**
	 * fe user
	 *
	 * @var \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser
	 */
	protected $feUser = NULL;
	
	/**
	 * fe group
	 *
	 * @var \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUserGroup
	 */
	protected $feGroup = NULL;
	
	/**
	 * variant
	 *
	 * @var \string
	 */
    protected $variant;
	
	/**
	 * title
	 *	
	 * @var \string
	 * @validate NotEmpty
	 */
	protected $title;
	
	/**
	 * description
	 *
	 * @var \string	
	 */
	protected $description;
	
	/**
	 * categories
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\Category>
	 * @lazy
	 */
	protected $categories;
	
	/**
	 * images
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\ImageReference>
	 * @lazy
	 */
	protected $images;
	
	/**
	 * files
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\FileReference>
	 * @lazy
	 */
	protected $files;
	
	/**
	 * paid
	 *
	 * @var \int
	 */
    protected $paid;
	
	/**
	 * paid by
	 *
	 * @var \string
	 */
    protected $paidBy;
	
	/**
	 * paid info
	 *
	 * @var \string
	 */
    protected $paidInfo;
	
	/**
	 * initialize object
	 *
	 * @return void
	 */
	public function initializeObject() {
		
		$this->initStorageObjects();
		
		// get init storage functions from extender extensions
		$extendedFunctions = array();
		$functions = get_class_methods(get_class($this));
		foreach($functions AS $function){
			if(substr($function,0,19)=="initStorageObjects_"){
				$this->$function();				
			}
		}		
	}
	
	/**
	 * Initializes all ObjectStorage properties	
	 *
	 * @return void
	 */
	protected function initStorageObjects() {		
		$this->categories = new ObjectStorage();
		$this->images = new ObjectStorage();
		$this->files = new ObjectStorage();
	}
	
	/**
     * get uid
	 *
     * @return \integer $uid uid
     */
    public function getUid() {
       return $this->uid;
    }
     
    /**
     * set uid
	 *
     * @param \integer $uid uid
	 * @return void
     */
    public function setUid($uid) {
        $this->uid = $uid;
    }
	
	/**
     * get pid
	 *
     * @return \integer $pid pid
     */
    public function getPid() {
       return $this->pid;
    }
     
    /**
     * set pid
	 *
     * @param \integer $pid pid
	 * @return void
     */
    public function setPid($pid) {
        $this->pid = $pid;
    }
	
	/**
     * get tstamp
	 *
     * @return \integer $tstamp tstamp
     */
    public function getTstamp() {
       return $this->tstamp;
    }
     
    /**
     * set tstamp
	 *
     * @param \integer $tstamp tstamp
	 * @return void
     */
    public function setTstamp($tstamp) {
        $this->tstamp = $tstamp;
    }		
	
	/**
     * get starttime
	 *
     * @return \integer $starttime starttime
     */
    public function getStarttime() {
       return $this->starttime;
    }
     
    /**
     * set starttime
	 *
     * @param \integer $starttime starttime
	 * @return void
     */
    public function setStarttime($starttime) {
        $this->starttime = $starttime;
    }
	
	/**
     * get endtime
	 *
     * @return \integer $endtime endtime
     */
    public function getEndtime() {
       return $this->endtime;
    }
     
    /**
     * set endtime
	 *
     * @param \integer $endtime endtime
	 * @return void
     */
    public function setEndtime($endtime) {
        $this->endtime = $endtime;
    }
	
	/**
     * get crdate
	 *
     * @return \integer $crdate crdate
     */
    public function getCrdate() {
       return $this->crdate;
    }
     
    /**
     * set crdate
	 *
     * @param \integer $crdate crdate
	 * @return void
     */
    public function setCrdate($crdate) {
        $this->crdate = $crdate;
    }
	
	/**
	 * Get year of crdate
	 *
	 * @return \integer
	 */
	public function getYearOfCrdate() {
		return $this->getCrdate()->format('Y');
	}

	/**
	 * Get month of crdate
	 *
	 * @return \integer
	 */
	public function getMonthOfCrdate() {
		return $this->getCrdate()->format('m');
	}

	/**
	 * Get day of crdate
	 *
	 * @return \integer
	 */
	public function getDayOfCrdate() {
		return (int)$this->crdate->format('d');
	}
	
	/**
	 * Returns the hidden
	 *
	 * @return \integer $hidden
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * Sets the hidden
	 *
	 * @param \integer $hidden
	 * @return void
	 */
	public function setHidden($hidden) {
		$this->hidden = $hidden;
	}	

	/**
	 * Returns the fe user
	 *
	 * @return \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser $feUser
	 */
	public function getFeUser() {
		return $this->feUser;
	}

	/**
	 * Sets the fe user
	 *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser $feUser
	 * @return void
	 */
	public function setFeUser(\DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser $feUser) {
		$this->feUser = $feUser;
	}
		
	/**
	 * Returns the fe group
	 *
	 * @return \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUserGroup $feGroup
	 */
	public function getFeGroup() {
		return $this->feGroup;
	}

	/**
	 * Sets the fe group
	 *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUserGroup $feGroup
	 * @return void
	 */
	public function setFeGroup(\DCNGmbH\MooxMarketplace\Domain\Model\FrontendUserGroup $feGroup) {
		$this->feGroup = $feGroup;
	}
	
	/**
     * set variant
	 *
     * @param \string $variant variant
	 * @return void
     */
    public function setVariant($variant) {
        $this->variant = $variant;
    }
	
	/**
     * get variant
	 *
     * @return \string $variant variant
     */
    public function getVariant() {
	   return $this->variant;
    }
	
	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * Returns the description
	 *
	 * @return \string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param \string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * Adds a category
	 *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategories(\DCNGmbH\MooxMarketplace\Domain\Model\Category $category) {
		$this->categories->attach($category);
	}

	/**
	 * Removes a category
	 *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\Category $category
	 * @return void
	 */
	public function removeCategories(\DCNGmbH\MooxMarketplace\Domain\Model\Category $category) {
		$this->categories->detach($category);
	}

	/**
	 * Returns the categories
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\Category> $categories
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Sets the categories
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\Category> $categories
	 * @return void
	 */
	public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories) {
		$this->categories = $categories;
	}
	
	/**
	 * sets the images
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\ImageReference> $images
	 *
	 * @return void
	 */
	public function setImages($images) {
		$this->images = $images;
	}
	 
	/**
	 * get the images
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\ImageReference>
	 */
	public function getImages() {
		return $this->images;
	}

	/**
     * adds an image
     *
     * @param \DCNGmbH\MooxMarketplace\Domain\Model\ImageReference $image
     *
     * @return void
     */
    public function addImages(\DCNGmbH\MooxMarketplace\Domain\Model\ImageReference $image) {
        $this->images->attach($image);
    }	
	
	/**
     * remove a image
     *
     * @param \DCNGmbH\MooxMarketplace\Domain\Model\ImageReference $image
     *
     * @return void
     */
    public function removeImages(\DCNGmbH\MooxMarketplace\Domain\Model\ImageReference $image) {
        $this->images->detach($image);
    }		
	
	/**
	 * sets the files
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\FileReference> $files
	 *
	 * @return void
	 */
	public function setFiles($files) {
		$this->files = $files;
	}
	 
	/**
	 * get the files
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\FileReference>
	 */
	public function getFiles() {
		return $this->files;
	}

	/**
     * adds a file
     *
     * @param \DCNGmbH\MooxMarketplace\Domain\Model\FileReference $file
     *
     * @return void
     */
    public function addFiles(\DCNGmbH\MooxMarketplace\Domain\Model\FileReference $file) {
        $this->files->attach($file);
    }	
	
	/**
     * remove a file
     *
     * @param \DCNGmbH\MooxMarketplace\Domain\Model\FileReference $file
     *
     * @return void
     */
    public function removeFiles(\DCNGmbH\MooxMarketplace\Domain\Model\FileReference $file) {
        $this->files->detach($file);
    }
	
	/**
     * get paid
	 *
     * @return \integer $paid paid
     */
    public function getPaid() {
		return $this->paid;
    }
     
    /**
     * set paid
	 *
     * @param \integer $paid paid
	 * @return void
     */
    public function setPaid($paid) {
		$this->paid = $paid;
    }
	
	/**
     * get paid by
	 *
     * @return \string $paidBy paid by
     */
    public function getPaidBy() {		     
	  return $this->paidBy;
    }
     
    /**
     * set paid by
	 *
     * @param \string $paidBy paid by
	 * @return void
     */
    public function setPaidBy($paidBy) {
        $this->paidBy = $paidBy;
    }
	
	/**
     * get paid info
	 *
     * @return \string $paidInfo paid info
     */
    public function getPaidInfo() {		     
	  return $this->paidInfo;
    }
     
    /**
     * set paid info
	 *
     * @param \string $paidInfo paid info
	 * @return void
     */
    public function setPaidInfo($paidInfo) {
        $this->paidInfo = $paidInfo;
    }
}
?>