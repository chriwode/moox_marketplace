<?php
namespace DCNGmbH\MooxMarketplace\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;  
 
/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Category extends AbstractEntity {
	
	/**
	 * @var \integer
	 */
	protected $sorting;

	/**
	 * @var \integer
	 */
	protected $crdate;

	/**
	 * @var \integer
	 */
	protected $tstamp;

	/**
	 * @var \integer
	 */
	protected $starttime;

	/**
	 * @var \boolean
	 */
	protected $hidden;

	/**
	 * @var \integer
	 */
	protected $endtime;

	/**
	 * @var \integer
	 */
	protected $sysLanguageUid;

	/**
	 * @var \integer
	 */
	protected $l10nParent;
	
	/**
	 * @var \string
	 */
	protected $variant;
	
	/**
	 * @var \string
	 */
	protected $title;

	/**
	 * @var \string
	 */
	protected $description;

	/**
	 * @var \DCNGmbH\MooxMarketplace\Domain\Model\Category
	 * @lazy
	 */
	protected $parentcategory;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplace\Domain\Model\ImageReference>
	 * @lazy
	 */
	protected $images;

	/**
	 * @var \string
	 */
	protected $classifiedVariants;

	
	/**
	 * keep it as string as it should be only used during imports
	 * @var string
	 */
	public $feGroup;

	/**
	 * Get creation date
	 *
	 * @return DateTime
	 */
	public function getCrdate() {
		return $this->crdate;
	}

	/**
	 * Set Creation Date
	 *
	 * @param \integer $crdate crdate
	 * @return void
	 */
	public function setCrdate($crdate) {
		$this->crdate = $crdate;
	}

	/**
	 * Get Tstamp
	 *
	 * @return \integer
	 */
	public function getTstamp() {
		return $this->tstamp;
	}

	/**
	 * Set tstamp
	 *
	 * @param \integer $tstamp tstamp
	 * @return void
	 */
	public function setTstamp($tstamp) {
		$this->tstamp = $tstamp;
	}

	/**
	 * Get starttime
	 *
	 * @return \integer
	 */
	public function getStarttime() {
		return $this->starttime;
	}

	/**
	 * Set starttime
	 *
	 * @param \integer $starttime starttime
	 * @return void
	 */
	public function setStarttime($starttime) {
		$this->starttime = $starttime;
	}

	/**
	 * Get Endtime
	 *
	 * @return \integer
	 */
	public function getEndtime() {
		return $this->endtime;
	}

	/**
	 * Set Endtime
	 *
	 * @param \integer $endtime endttime
	 * @return void
	 */
	public function setEndtime($endtime) {
		$this->endtime = $endtime;
	}

	/**
	 * Get Hidden
	 *
	 * @return boolean
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * Set Hidden
	 *
	 * @param boolean $hidden
	 * @return void
	 */
	public function setHidden($hidden) {
		$this->hidden = $hidden;
	}

	/**
	 * Get sys language
	 *
	 * @return integer
	 */
	public function getSysLanguageUid() {
		return $this->_languageUid;
	}

	/**
	 * Set sys language
	 *
	 * @param integer $sysLanguageUid language uid
	 * @return void
	 */
	public function setSysLanguageUid($sysLanguageUid) {
		$this->_languageUid = $sysLanguageUid;
	}

	/**
	 * Get language parent
	 *
	 * @return integer
	 */
	public function getL10nParent() {
		return $this->l10nParent;
	}

	/**
	 * Set language parent
	 *
	 * @param integer $l10nParent l10nParent
	 * @return void
	 */
	public function setL10nParent($l10nParent) {
		$this->l10nParent = $l10nParent;
	}

	/**
	 * Get variant
	 *
	 * @return string
	 */
	public function getVariant() {
		return $this->variant;
	}

	/**
	 * Set variant
	 *
	 * @param string $variant variant
	 * @return void
	 */
	public function setVariant($variant) {
		$this->variant = $variant;
	}
	
	/**
	 * Get category title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set category title
	 *
	 * @param string $title title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set description
	 *
	 * @param string $description description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $images
	 */
	public function setImages($images) {
		$this->images = $images;
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * Add image
	 *
	 * @param Tx_MooxNews_Domain_Model_FileReference $image
	 */
	public function addImages(Tx_MooxNews_Domain_Model_FileReference $image) {
		$this->images->attach($image);
	}

	/**
	 * Remove image
	 *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\ImageReference $image
	 */
	public function removeImages(\DCNGmbH\MooxMarketplace\Domain\Model\ImageReference $image) {
		$this->images->detach($image);
	}

	/**
	 * Get the first image
	 *
	 * @return \DCNGmbH\MooxMarketplace\Domain\Model\ImageReference|null
	 */
	public function getFirstImage() {
		$images = $this->getImages();
		foreach($images as $image) {
			return $image;
		}

		return NULL;
	}

	/**
	 * Get parent category
	 *
	 * @return \DCNGmbH\MooxMarketplace\Domain\Model\Category
	 */
	public function getParentcategory() {
		return $this->parentcategory;
	}

	/**
	 * Set parent category
	 *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\Category $category parent category
	 * @return void
	 */
	public function setParentcategory(\DCNGmbH\MooxMarketplace\Domain\Model\Category $category) {
		$this->parentcategory = $category;
	}	
	
	/**
	 * Get classified variants
	 *
	 * @return string
	 */
	public function getClassifiedVariants() {
		return $this->classifiedVariants;
	}

	/**
	 * Set classified variants
	 *
	 * @param string $classifiedVariants classified variants
	 * @return void
	 */
	public function setClassifiedVariants($classifiedVariants) {
		$this->classifiedVariants = $classifiedVariants;
	}
	
	/**
	 * Get sorting
	 *
	 * @return integer sorting
	 */
	public function getSorting() {
		return $this->sorting;
	}

	/**
	 * Set sorting
	 *
	 * @param integer $sorting sorting
	 * @return void
	 */
	public function setSorting($sorting) {
		$this->sorting = $sorting;
	}

	/**
	 * Get fe group
	 *
	 * @return string
	 */
	public function getFeGroup() {
		return $this->feGroup;
	}

	/**
	 * Set fe group
	 *
	 * @param \string $feGroup feGroup
	 * @return void
	 */
	public function setFeGroup($feGroup) {
		$this->feGroup = $feGroup;
	}	
}
