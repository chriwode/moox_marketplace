<?php
namespace DCNGmbH\MooxMarketplace\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;

/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi2Controller extends AbstractController
{
    /**
     * categories
     *
     * @var array
     */
    protected $categories;
    
    /**
     * variant
     *
     * @var array
     */
    protected $variants;
        
    /**
     * Path to the locallang file
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang.xlf:';
        
    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        parent::initializeAction();
        
        // set categories
        if ($this->settings['categories'] != '') {
            $this->categories = GeneralUtility::trimExplode(',', $this->settings['categories'], true);
        } else {
            $this->categories = null;
        }
        
        // set variant
        if ($this->settings['variants'] != '') {
            $this->variants = GeneralUtility::trimExplode(',', $this->settings['variants'], true);
        } else {
            $this->variants = array();
        }
    }
    
    /**
     * action list
     *
     * @param array $filter
     * @return void
     */
    public function listAction($filter = null)
    {
        $order['by'] = key($this->orderings);
        $order['dir'] = current($this->orderings);
        
        // get fields set by plugin
        $fields = $this->helperService->configureFields(
            $this->settings['listFields'],
            '',
            $this->settings,
            $this->fieldConfig
        );
        
        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields(
            $this->allowedOrderBy,
            '',
            $this->settings,
            $this->fieldConfig
        );
        
        // set paid filter
        $filter['paid'] = true;
        
        // set categories filter
        if ($this->categories) {
            $filter['categories'] = $this->categories;
        }
        
        // set variant filter
        if (count($this->variants)) {
            $filter['variants'] = $this->variants;
        }
        
        // get items
        $items = $this->classifiedRepository->findByFilter(
            $filter,
            $this->orderings,
            null,
            null,
            $this->storagePids,
            null
        );

        $assignMultiple = [
            'fields' => $fields,
            'filter' => $filter,
            'order' => $order,
            'orderByFields' => $orderByFields,
            'pagination' => $this->pagination,
            //'categories' => $categories,
            'items' => $items,
            'returnUrl' => $this->uriBuilder->getRequest()->getRequestUri(),
            'action' => 'list'
        ];
        $this->view->assignMultiple($assignMultiple);
    }
    
    /**
     * action detail
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function detailAction($item = null, $returnUrl = null)
    {
        $messages = [];
        $readable = false;
        
        // check and get logged in user
        if ($item > 0) {
            $item = $this->classifiedRepository->findByUid($item);
            $readable = true;
        }
       
        // if item is readable by current user
        if (!$readable) {
            $messages[] = [
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi2.action_detail', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi2.action_detail.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            ];
            $this->helperService->setFlashMessages($this, $messages);
            $this->redirect('error');
        }

        $fields = $this->helperService->configureFields(
            $this->settings['detailFields'],
            '',
            $this->settings,
            $this->fieldConfig,
            $item->getVariant()
        );

        $multipleAssign = [
            'item' => $item,
            'fields' => $fields,
            'returnUrl' => $returnUrl,
            'action' => 'detail'
        ];
        $this->view->assignMultiple($multipleAssign);
    }
    
    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }
}
