<?php

namespace DCNGmbH\MooxMarketplace\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Christian Wolfram <c.wolfram@chriwo.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;

/**
 * Class AbstractController
 *
 * @package moox_marketplace
 */
class AbstractController extends ActionController
{
    /**
     * accessControllService
     *
     * @var \DCNGmbH\MooxMarketplace\Service\AccessControlService
     * @inject
     */
    protected $accessControllService;

    /**
     * classifiedRepository
     *
     * @var \DCNGmbH\MooxMarketplace\Domain\Repository\ClassifiedRepository
     * @inject
     */
    protected $classifiedRepository;

    /**
     * frontendUserRepository
     *
     * @var \DCNGmbH\MooxMarketplace\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * frontendUserGroupRepository
     *
     * @var \DCNGmbH\MooxMarketplace\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository;

    /**
     * categoryRepository
     *
     * @var \DCNGmbH\MooxMarketplace\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var \DCNGmbH\MooxCommunity\Domain\Repository\AboMembershipRepository
     * @inject
     */
    protected $aboRepository;

    /**
     * helperService
     *
     * @var \DCNGmbH\MooxMarketplace\Service\HelperService
     * @inject
     */
    protected $helperService;

    /**
     * extConf
     *
     * @var array
     */
    protected $extConf;

    /**
     * storagePids
     *
     * @var array
     */
    protected $storagePids;

    /**
     * fields
     *
     * @var array
     */
    protected $fields;

    /**
     * fieldConfig
     *
     * @var array
     */
    protected $fieldConfig;

    /**
     * pagination
     *
     * @var array
     */
    protected $pagination;

    /**
     * orderings
     *
     * @var array
     */
    protected $orderings;

    /**
     * variant
     *
     * @var string
     */
    protected $variant;

    /**
     * @var array
     */
    protected $typoScriptConfiguration;

    /**
     * allowedOrderBy
     *
     * @var string
     */
    protected $allowedOrderBy;

    /**
     * @var object
     */
    protected $feUser = null;

    /**
     * Initialize action
     *
     * @return void
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public function initializeAction()
    {
        parent::initializeAction();

        $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace']);

        if ($this->settings['itemsPerPage'] > 0) {
            $this->pagination['itemsPerPage'] = $this->settings['itemsPerPage'];
        } else {
            $this->pagination['itemsPerPage'] = 10000;
        }

        if ($this->settings['orderBy'] != '') {
            $orderBy = $this->settings['orderBy'];
            $this->allowedOrderBy = $orderBy;
        } else {
            if ($this->settings['allowedOrderBy'] != '') {
                $allowedOrderBy = GeneralUtility::trimExplode(',', $this->settings['allowedOrderBy'], true);
                $orderBy = $allowedOrderBy[0];
                $this->allowedOrderBy = $this->settings['allowedOrderBy'];
            } else {
                $orderBy = 'crdate';
                $this->allowedOrderBy = $orderBy;
            }
        }

        if ($this->settings['orderDirection'] != '') {
            $orderDirection = $this->settings['orderDirection'];
        } else {
            $orderDirection = 'ASC';
        }
        $this->orderings = [$orderBy => $orderDirection];

        $this->fields = $this->helperService->getPluginFields(
            'tx_mooxmarketplace_domain_model_classified',
            'mooxmarketplace',
            'all'
        );

        $this->fieldConfig = $this->helperService->getFieldConfig($this->fields, $this->settings);

        $this->typoScriptConfiguration = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        );
        $this->initializeStorageSettings($this->typoScriptConfiguration);

        // check and get logged in user
        if (true === $this->accessControllService->hasLoggedInFrontendUser()) {
            $this->feUser = $this->frontendUserRepository->findByUid(
                $this->accessControllService->getFrontendUserUid()
            );
        }
    }

    /**
     * initialize storage settings
     *
     * @param array $configuration
     * @return void
     */
    protected function initializeStorageSettings($configuration = [])
    {
        // set storage pid if set by plugin
        if ($this->settings['storagePid']!='') {
            $this->setStoragePids(array($this->settings['storagePid']));
        } else {
            $this->setStoragePids(array());
        }
        $this->helperService->setStoragePids($this->getStoragePids());

        // overwrite extbase persistence storage pid
        if (!empty($this->settings['storagePid']) && $this->settings['storagePid']!='TS') {
            if (empty($configuration['persistence']['storagePid'])) {
                $storagePids['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration(array_merge($configuration, $storagePids));
            } else {
                $configuration['persistence']['storagePid'] = $this->settings['storagePid'];
                $this->configurationManager->setConfiguration($configuration);
            }
        }
    }

    /**
     * Returns storage pids
     *
     * @return array
     */
    public function getStoragePids()
    {
        return $this->storagePids;
    }

    /**
     * Set storage pids
     *
     * @param array $storagePids storage pids
     * @return void
     */
    public function setStoragePids($storagePids)
    {
        $this->storagePids = $storagePids;
    }

    /**
     * Returns fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set fields
     *
     * @param array $fields fields
     * @return void
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * Returns fieldConfig
     *
     * @return array
     */
    public function getFieldConfig()
    {
        return $this->fieldConfig;
    }

    /**
     * Set fieldConfig
     *
     * @param array $fieldConfig fieldConfig
     * @return void
     */
    public function setFieldConfig($fieldConfig)
    {
        $this->fieldConfig = $fieldConfig;
    }

    /**
     * Returns ext conf
     *
     * @return array
     */
    public function getExtConf()
    {
        return $this->extConf;
    }

    /**
     * Set ext conf
     *
     * @param array $extConf ext conf
     * @return void
     */
    public function setExtConf($extConf)
    {
        $this->extConf = $extConf;
    }

    /**
     * Debugs a SQL query from a QueryResult
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult
     * @param bool $explainOutput
     * @return void
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    public function debugQuery(QueryResult $queryResult, $explainOutput = false)
    {
        $GLOBALS['TYPO3_DB']->debugOuput = 2;
        if ($explainOutput) {
            $GLOBALS['TYPO3_DB']->explainOutput = true;
        }
        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
        $queryResult->toArray();
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($GLOBALS['TYPO3_DB']->debug_lastBuiltQuery);

        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = false;
        $GLOBALS['TYPO3_DB']->explainOutput = false;
        $GLOBALS['TYPO3_DB']->debugOuput = false;
    }
}
