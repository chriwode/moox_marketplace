<?php

namespace DCNGmbH\MooxMarketplace\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessage;

/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Pi1Controller extends AbstractController
{
    /**
     * persistenceManager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;

    /**
     * storageRepository
     *
     * @var \TYPO3\CMS\Core\Resource\StorageRepository
     * @inject
     */
    protected $storageRepository;

    /**
     * fileRepository
     *
     * @var \DCNGmbH\MooxMarketplace\Domain\Repository\FileRepository
     * @inject
     */
    protected $fileRepository;

    /**
     * imageReferenceRepository
     *
     * @var \DCNGmbH\MooxMarketplace\Domain\Repository\ImageReferenceRepository
     * @inject
     */
    protected $imageReferenceRepository;

    /**
     * fileReferenceRepository
     *
     * @var \DCNGmbH\MooxMarketplace\Domain\Repository\FileReferenceRepository
     * @inject
     */
    protected $fileReferenceRepository;

    /**
     * paymentService
     *
     * @var \DCNGmbH\MooxPayment\Service\PaymentService
     */
    protected $paymentService;

    /**
     * fileStorageUid
     *
     * @var int
     */
    protected $fileStorageUid;

    /**
     * fileStorageFolder
     *
     * @var int
     */
    protected $fileStorageFolder;

    /**
     * allowedVariants
     *
     * @var array
     */
    protected $allowedVariants;

    /**
     * partialRootPath
     *
     * @var string
     */
    protected $partialRootPath;

    /**
     * Path to the locallang file
     *
     * @var string
     */
    const LLPATH = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang.xlf:';

    /**
     * fe user variant
     *
     * @var string
     */
    const VARIANT = 'moox_marketplace';

    /**
     * temp folder
     *
     * @var string
     */
    const TEMPFOLDER = 'moox_marketplace';

    /**
     * initialize action
     *
     * @return void
     */
    public function initializeAction()
    {
        // execute parent initialize action
        parent::initializeAction();

        $this->settings['formKey'] = 'tx_mooxmarketplace_pi1';

        // init payment service
        if (ExtensionManagementUtility::isLoaded('moox_payment')) {
            $this->paymentService = $this->objectManager->get('DCNGmbH\MooxPayment\Service\PaymentService');
        }

        // set file storage uid
        if ($this->settings['fileStorage'] != '') {
            $this->fileStorageUid = $this->settings['fileStorage'];
        } else {
            $this->fileStorageUid = 1;
        }

        // set file storage folder
        if ($this->settings['fileStorageFolder'] != '') {
            $this->fileStorageFolder = $this->settings['fileStorageFolder'];
        } else {
            $this->fileStorageFolder = 'moox_marketplace';
        }

        // set variant
        if ($this->settings['variant'] != '') {
            $this->variant = $this->settings['variant'];
            $this->allowedVariants = [$this->variant => GeneralUtility::underscoredToUpperCamelCase($this->variant)];
        } else {
            if ($this->settings['allowedVariants'] != '') {
                $this->allowedVariants = [];
                $allowedVariants = GeneralUtility::trimExplode(',', $this->settings['allowedVariants'], true);
                foreach ($allowedVariants as $variant) {
                    $this->allowedVariants[$variant] = GeneralUtility::underscoredToUpperCamelCase($variant);
                }
            } else {
                $this->allowedVariants = [];
            }
            $this->variant = null;
        }

        // get current partial root path
        $this->partialRootPath = end($this->typoScriptConfiguration['view']['partialRootPaths']);
    }

    /**
     * action list
     *
     * @param array $filter
     * @return void
     */
    public function listAction($filter = null)
    {
        // init action arrays and booleans
        $messages = [];
        $paymentRequired = false;

        // if user is logged in
        if (is_null($this->feUser)) {
            $messages[] = [
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_list', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_list.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            ];

            $this->helperService->setFlashMessages($this, $messages);
            $this->redirect('error');
        }

        // check if user is allowed to add classified for free or paid
        if ($this->accessControllService->addAllowedWithoutPayment($this->feUser, $this->settings)) {
            $paymentRequired = false;
        } elseif ($this->accessControllService->addAllowedWithPayment($this->feUser, $this->settings)) {
            if (is_object($this->paymentService)) {
                $paymentMethods = $this->paymentService->getPaymentMethods($this->settings['paymentMethods']);
                if (count($paymentMethods)) {
                    $paymentRequired = true;
                }
            }
        }

        $order['by'] = key($this->orderings);
        $order['dir'] = current($this->orderings);

        // get fields set by plugin
        $fields = $this->helperService->configureFields(
            $this->settings['listFields'],
            '',
            $this->settings,
            $this->fieldConfig
        );

        // get order by fields set by plugin
        $orderByFields = $this->helperService->configureFields(
            $this->allowedOrderBy,
            '',
            $this->settings,
            $this->fieldConfig
        );

        // set fe user filter
        $filter['feUser'] = $this->feUser;

        // set variant filter
        if ($this->variant) {
            $filter['variant'] = $this->variant;
        } elseif ($this->allowedVariants) {
            $filter['variants'] = [];
            foreach ($this->allowedVariants as $variant => $label) {
                $filter['variants'][] = $variant;
            }
        }

        // get items
        $items = $this->classifiedRepository->findByFilter(
            $filter,
            $this->orderings,
            null,
            null,
            $this->storagePids,
            ['disabled']
        );

        // set flash messages
        $this->helperService->setFlashMessages($this, $messages);

        // set template variables
        $this->view->assign('fields', $fields);
        $this->view->assign('filter', $filter);
        $this->view->assign('order', $order);
        $this->view->assign('orderByFields', $orderByFields);
        $this->view->assign('pagination', $this->pagination);
        $this->view->assign('items', $items);
        $this->view->assign('allowedVariants', $this->allowedVariants);
        $this->view->assign('paymentRequired', $paymentRequired);
        $this->view->assign('returnUrl', $this->uriBuilder->getRequest()->getRequestUri());
        $this->view->assign('action', 'list');
    }

    /**
     * action detail
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function detailAction($item = null, $returnUrl = null)
    {
        $messages = [];
        $readable = false;

        // check and get logged in user
        if ($item > 0) {
            $item = $this->classifiedRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxMarketplace\Domain\Model\Classified')) {
            if ($this->feUser->getUid() == $item->getFeUser()->getUid()) {
                $readable = true;
            }
        }

        // if item is readable by current user
        if (!$readable) {
            $messages[] = [
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_detail', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_detail.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            ];

            $this->helperService->setFlashMessages($this, $messages);
            $this->redirect('error');
        }

        $fields = $this->helperService->configureFields(
            $this->settings['detailFields'],
            '',
            $this->settings,
            $this->fieldConfig,
            $item->getVariant()
        );

        $multipleAssign = [
            'item' => $item,
            'fields' => $fields,
            'returnUrl' => $returnUrl,
            'action' => 'detail'
        ];
        $this->view->assignMultiple($multipleAssign);
    }

    /**
     * action add
     *
     * @param array $add
     * @param string $variant
     * @param array $payment
     * @param string $returnUrl
     * @param bool $reload
     * @return void
     */
    public function addAction($add = null, $variant = null, $payment = null, $returnUrl = null, $reload = false)
    {
        $messages = [];
        $errors = [];
        $fields = [];
        $fileAddOnly = false;
        $fileRemoveOnly = false;
        $paymentMethods = null;

        // set variant
        if (isset($add['variant']) && $add['variant'] != '') {
            $variant = $add['variant'];
        } elseif ($variant != ''
            && in_array(GeneralUtility::underscoredToUpperCamelCase($variant), $this->allowedVariants)
            && (!$this->variant || in_array($variant, explode(',', $this->variant)))
        ) {
            $variant = $variant;
        } elseif ($this->variant) {
            $variant = $this->variant;
        } else {
            $variant = GeneralUtility::camelCaseToLowerCaseUnderscored($this->allowedVariants[0]);
        }

        // if user is logged in and at least one variant is allowed
        if (is_null($this->feUser) || empty($variant)) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_add', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_add.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );
            $this->helperService->setFlashMessages($this, $messages);
            $this->redirect('error');
        }

        $paymentRequired = $this->accessControllService->checkPaymentStatus($this->feUser, $variant);
        if ($paymentRequired) {
            $paymentMethods = $this->paymentService->getPaymentMethods($this->settings['paymentMethods']);
        }

        // get fields set by plugin (consider variant)
        $fields = $this->helperService->configureFields(
            $this->settings['addFields'],
            $this->settings['requiredAddFields'],
            $this->settings,
            $this->fieldConfig,
            $variant,
            $this->allowedVariants
        );

        // process form values
        if ($add && !$reload) {
            // add or remove temp files by add button
            foreach ($fields as $field) {

                // if field is a file field
                if ($field['config']['type'] == 'file') {

                    // if temporary file form fields are set
                    if (is_array($add[$field['key'] . '_tmp'])) {

                        // go through all temporary file form fields
                        foreach ($add[$field['key'] . '_tmp'] as $key => $value) {

                            // if remove button for this temporary field is set
                            if (isset($add[$field['key'] . '_remove_' . $key])) {

                                // remove this temporary file form field from form
                                unset($add[$field['key'] . '_tmp'][$key]);

                                // add message
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_add',
                                                                              $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_all.removed.' . $field['config']['reference-type'],
                                                                             $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );

                                // set "file remove only" to prevent further processing of form values
                                $fileRemoveOnly = true;
                            }
                        }
                    }

                    // if form file add button was pressed for this field
                    if (isset($add[$field['key']]) && isset($add[$field['key'] . '_add'])) {

                        // if file is selected in this form file field
                        if ($add[$field['key']]['name'] != '') {

                            // get current count of temporary file form fields for this file field
                            $fileCnt = (is_array($add[$field['key'] . '_tmp'])) ? count($add[$field['key'] . '_tmp']
                            ) : 0;

                            // if maxitems not reached yet for this field
                            if (!isset($field['config']['maxitems']) || (isset($field['config']['maxitems']) && $fileCnt < $field['config']['maxitems'])) {

                                // get file information for selected file
                                $fileInfo = $this->helperService->getFileInfo($add[$field['key']]['name']);

                                // set file name to store
                                $fileName = $fileInfo['name'] . '.' . $fileInfo['extension'];

                                // make temporary copy of file in typo3 tempfolder
                                $fileTemp = $this->helperService->copyToTemp($add[$field['key']]['tmp_name'], $fileName,
                                                                             self::TEMPFOLDER
                                );

                                // set temporary file form fields for added file
                                $add[$field['key'] . '_tmp'][$fileCnt]['name'] = $fileName;
                                $add[$field['key']]['tmp_name_local'] = $fileTemp['path'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['src'] = $fileTemp['src'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['ext'] = $fileInfo['extension'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['size'] = $add[$field['key']]['size'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['object'] = serialize($add[$field['key']]);

                                // add message
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_add',
                                                                              $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_all.added.' . $field['config']['reference-type'],
                                                                             $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );
                            } else {

                                // add message
                                $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_many',
                                                                          $this->extensionName,
                                                                          array($field['config']['maxitems'])
                                );
                                if ($message == '') {
                                    $message = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                          $field['extkey'], self::LLPATH
                                                                              ) . 'form.' . $field['key'] . '.error.too_many',
                                                                              $field['extkey'],
                                                                              array($field['config']['maxitems'])
                                    );
                                }
                                if ($message == '') {
                                    $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_many',
                                                                              $this->extensionName,
                                                                              array($field['config']['maxitems'])
                                    );
                                }
                                $title = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'],
                                                                        $this->extensionName
                                );
                                if ($title == '') {
                                    $title = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                        $field['extkey'], self::LLPATH
                                                                            ) . 'form.' . $field['key'],
                                                                            $field['extkey']
                                    );
                                }
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => $title,
                                    'text' => $message,
                                    'type' => FlashMessage::ERROR
                                );

                                // set error
                                $errors[$field['key']] = true;
                            }
                        } else {

                            // add message
                            $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.empty',
                                                                      $this->extensionName
                            );
                            if ($message == '') {
                                $message = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                      $field['extkey'], self::LLPATH
                                                                          ) . 'form.' . $field['key'] . '.error.empty',
                                                                          $field['extkey']
                                );
                            }
                            if ($message == '') {
                                $message = LocalizationUtility::translate(self::LLPATH . 'form.error.empty',
                                                                          $this->extensionName
                                );
                            }
                            $title = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'],
                                                                    $this->extensionName
                            );
                            if ($title == '') {
                                $title = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                    $field['extkey'], self::LLPATH
                                                                        ) . 'form.' . $field['key'], $field['extkey']
                                );
                            }
                            $messages[] = array(
                                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                'title' => $title,
                                'text' => $message,
                                'type' => FlashMessage::ERROR
                            );

                            // set error
                            $errors[$field['key']] = true;
                        }

                        // set "file add only" to prevent further processing of form values
                        $fileAddOnly = true;
                    }
                }
            }

            // if general save button is pressed
            if (!$fileAddOnly && !$fileRemoveOnly) {
                $this->helperService->checkFields($fields, $add, $messages, $errors);

                // check payment if needed
                if ($paymentRequired) {
                    $payment['voucher_target'] = 'tx_mooxmarketplace_domain_model_classified';
                    $paid = $this->helperService->checkPayment($payment, $messages, $errors);
                }

                // set item values
                $item = $this->objectManager->get('DCNGmbH\MooxMarketplace\Domain\Model\Classified');
                foreach ($fields as $field) {
                    if (!$field['passhrough'] && !in_array($field['config']['type'], array('file')
                        ) && !in_array($field['key'], array('variant', 'categories'))
                    ) {
                        $setMethod = 'set' . GeneralUtility::underscoredToUpperCamelCase($field['key']);
                        if (method_exists($item, $setMethod) && isset($add[$field['key']])) {
                            if (in_array($field['config']['type'], array('date'))) {
                                $item->$setMethod($this->helperService->getTimestamp($add[$field['key']]));
                            } elseif (in_array($field['config']['type'], array('datetime'))) {
                                $item->$setMethod($this->helperService->getTimestamp($add[$field['key']],
                                                                                     $add[$field['key'] . '_hh'],
                                                                                     $add[$field['key'] . '_mm']
                                )
                                );
                            } else {
                                $item->$setMethod((is_array($add[$field['key']])) ? implode(',', $add[$field['key']]
                                ) : trim($add[$field['key']])
                                );
                            }
                        }
                    }
                }

                // no errors -> add classified
                if (!count($errors)) {

                    // set item variant
                    $item->setVariant($variant);

                    // set file relations
                    foreach ($fields as $field) {

                        // if field is a file field
                        if (in_array($field['config']['type'], array('file'))) {

                            // if file is selected in this form file field
                            if ($add[$field['key']]['name'] != '') {

                                // get current count of temporary file form fields for this file field
                                $fileCnt = (is_array($add[$field['key'] . '_tmp'])) ? count($add[$field['key'] . '_tmp']
                                ) : 0;

                                // get file information for selected file
                                $fileInfo = $this->helperService->getFileInfo($add[$field['key']]['name']);

                                // set file name to store
                                $fileName = $fileInfo['name'] . '.' . $fileInfo['extension'];

                                // make temporary copy of file in typo3 tempfolder
                                $fileTemp = $this->helperService->copyToTemp($add[$field['key']]['tmp_name'], $fileName,
                                                                             self::TEMPFOLDER
                                );

                                // set temporary file form fields for added file
                                $add[$field['key'] . '_tmp'][$fileCnt]['name'] = $fileName;
                                $add[$field['key']]['tmp_name_local'] = $fileTemp['path'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['src'] = $fileTemp['src'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['ext'] = $fileInfo['extension'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['size'] = $add[$field['key']]['size'];
                                $add[$field['key'] . '_tmp'][$fileCnt]['object'] = serialize($add[$field['key']]);
                            }

                            // if any temporary files are selected for this field
                            if (isset($add[$field['key'] . '_tmp']) && is_array($add[$field['key'] . '_tmp']
                                ) && count($add[$field['key'] . '_tmp'])
                            ) {

                                // prepare get method call
                                $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                // prepare add method call
                                $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                // if get and add methods are existing
                                if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {

                                    // get file storage
                                    $storage = $this->storageRepository->findByUid($this->fileStorageUid);

                                    // create file storage folder if not already existing
                                    if (!$storage->hasFolder($this->fileStorageFolder)) {
                                        $folder = $storage->createFolder($this->fileStorageFolder);
                                    } else {
                                        $folder = $storage->getFolder($this->fileStorageFolder);
                                    }

                                    // go through all temporary files
                                    foreach ($add[$field['key'] . '_tmp'] as $tmpFile) {

                                        // get real file form object from temporary file
                                        $tmpFile['object'] = unserialize($tmpFile['object']);

                                        // add file to storage
                                        $fileObject = $storage->addFile(
                                            $tmpFile['object']['tmp_name_local'], $folder, $tmpFile['object']['name']
                                        );

                                        // get recently added file object from storage
                                        $fileObject = $storage->getFile($fileObject->getIdentifier());

                                        // get real file object from file repo
                                        $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));

                                        // set reference type depending on field
                                        if ($field['config']['reference-type'] == 'image') {
                                            $referenceType = 'ImageReference';
                                        } else {
                                            $referenceType = 'FileReference';
                                        }

                                        // set reference extkey
                                        if ($field['config']['extkeyUCC'] && $field['config']['extkeyUCC'] != 'MooxMarketplace') {
                                            $referenceExtkey = $field['config']['extkeyUCC'];
                                        } else {
                                            $referenceExtkey = 'MooxMarketplace';
                                        }

                                        // get new file reference object to reference the added file
                                        $fileReference = $this->objectManager->get('DCNGmbH\\' . $referenceExtkey . '\Domain\Model\\' . $referenceType
                                        );
                                        $fileReference->setFile($file);
                                        $fileReference->setCruserId(0);

                                        // add new file reference to classified
                                        $item->$addMethod($fileReference);

                                        // remove temporary files in typo3 tempfolder
                                        if (file_exists($tmpFile['object']['tmp_name_local'])) {
                                            unlink($tmpFile['object']['tmp_name_local']);
                                        }
                                    }
                                }
                            }
                            // if field is the categories field
                        } elseif ($field['key'] == 'categories') {

                            // if file is selected in this form file field
                            if (isset($add[$field['key']])) {

                                // if file is selected in this form file field
                                if ($add[$field['key']] != '') {

                                    // prepare get method call
                                    $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare add method call
                                    $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // if get and add methods are existing
                                    if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {

                                        // get selected categories from form
                                        $formItems = explode(',', $add[$field['key']]);

                                        // add new elements
                                        foreach ($formItems as $formItem) {
                                            $object = $this->categoryRepository->findByUid($formItem);
                                            if (is_object($object)) {
                                                $item->$addMethod($object);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // set fe user
                    $item->setFeUser($this->feUser);

                    // set pid (bugfix ignoring global setting);
                    if (!$item->getPid()) {
                        $item->setPid($this->settings['storagePid']);
                    }

                    // set expirations date
                    if ((int)$this->settings['expiresAfterXDays'] > 0) {
                        $expires = mktime(0, 0, 0, date('n'), date('j') + (int)$this->settings['expiresAfterXDays'],
                                          date('Y')
                        );
                        $item->setEndtime($expires);
                    }

                    // set payment
                    if ($paymentRequired) {
                        if ($paid) {
                            $item->setPaid(time());
                        }
                        $item->setPaidBy($payment['method']);
                    } else {
                        $item->setPaid(time());
                        $item->setPaidBy('user_rights');

                        /**@var \DCNGmbH\MooxCommunity\Domain\Model\AboMembership $abo*/
                        $abo = $this->feUser->getAboMembership()->current();
                        $abo->setUsedMarketplace($abo->getUsedMarketplace() + 1);
                        $this->aboRepository->update($abo);
                    }

                    // save item to database
                    $this->classifiedRepository->add($item);
                    $this->persistenceManager->persistAll();

                    // execute process datamap after database operations if available
                    if (class_exists("DCNGmbH\MooxMarketplace\Hooks\Tcemain")) {
                        $Tcemain = $this->objectManager->get('DCNGmbH\\MooxMarketplace\\Hooks\\Tcemain');
                        if (is_object($Tcemain)) {
                            $Tcemain->processDatamap_afterDatabaseOperations(null,
                                                                             'tx_mooxmarketplace_domain_model_classified',
                                                                             (string)$item->getUid(), array(), null
                            );
                        }
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_add', $this->extensionName
                        ),
                        'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_add.success',
                                                                 $this->extensionName
                        ),
                        'type' => FlashMessage::OK
                    );

                    // set flash messages
                    $this->helperService->setFlashMessages($this, $messages);

                    // redirect to list view/payment
                    if ($returnUrl) {
                        if ($paymentRequired && !$paid) {
                            $redirectUri = $this->uriBuilder->setNoCache(1)->uriFor('payment',
                                                                                    array('item' => $item->getUid(
                                                                                    ), 'payment' => $payment, 'returnUrl' => $returnUrl),
                                                                                    'Pi1', 'MooxMarketplace', 'Pi1'
                            );
                            $this->redirectToUri($redirectUri);
                        } else {
                            $this->redirectToUri($returnUrl);
                        }
                    } else {
                        if ($paymentRequired && !$paid) {
                            $redirectUri = $this->uriBuilder->setNoCache(1)->uriFor('payment',
                                                                                    array('item' => $item->getUid(
                                                                                    ), 'payment' => $payment), 'Pi1',
                                                                                    'MooxMarketplace', 'Pi1'
                            );
                            $this->redirectToUri($redirectUri);
                        } else {
                            $this->redirect('list');
                        }
                    }

                    // unser register object
                    unset($add);
                }
            }
        } elseif (!$reload) {
            foreach ($fields as $field) {
                if ($field['config']['default'] != '') {
                    $add[$field['key']] = $field['config']['default'];
                }
            }
        }

        // set default payment method
        if ($paymentRequired && !isset($payment['method'])) {
            $payment['method'] = current($paymentMethods)['extkey'];
        }

        // set default variant
        if (!isset($add['variant'])) {
            $add['variant'] = $variant;
        }

        // set flash messages
        $this->helperService->setFlashMessages($this, $messages);
        $assignMultiple = [
            'add' => $add,
            'fields' => $fields,
            'errors' => $errors,
            'partialRootPath' => $this->partialRootPath,
            'paymentMethods' => $paymentMethods,
            'paymentRequired' => $paymentRequired,
            'payment' => $payment,
            'returnUrl' => $returnUrl,
            'action' => 'add'
        ];
        $this->view->assignMultiple($assignMultiple);
    }

    /**
     * action payment
     *
     * @param int $item
     * @param array $payment
     * @param string $returnUrl
     * @param bool $process
     * @param bool $check
     * @return void
     */
    public function paymentAction($item = null, $payment = null, $returnUrl = null, $process = false, $check = false)
    {

        // init action arrays and booleans
        $messages = [];
        $errors = [];

        if ($item > 0) {

            // get payment methods
            if (is_object($this->paymentService)) {
                $paymentMethods = $this->paymentService->getPaymentMethods($this->settings['paymentMethods']);
            }

            $item = $this->classifiedRepository->findByUid($item, false);

            // get payment config
            $config = $this->helperService->getPaymentConfig(($payment['method'] != '') ? $payment['method'] : $item->getPaidBy(
            )
            );

            if (!is_null($payment)) {
                if (count($config) && count($payment)) {
                    $payment['amount'] = $this->settings['paymentAmount'];

                    if ($check) {
                        $payment['voucher_target'] = 'tx_mooxmarketplace_domain_model_classified';
                        $payment['voucher_target_uid'] = $item->getUid();
                        if ($this->helperService->checkPayment($payment, $messages, $errors)) {

                            // set paid state
                            $item->setPaid(time());
                            $item->setPaidBy($payment['method']);
                            $item->setPaidInfo('');

                            // save changes to database
                            $this->classifiedRepository->update($item);
                            $this->persistenceManager->persistAll();

                            // add message
                            $messages[] = array(
                                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_payment',
                                                                          $this->extensionName
                                ),
                                'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_payment.success',
                                                                         $this->extensionName
                                ),
                                'type' => FlashMessage::OK
                            );

                            // set flash messages
                            $this->helperService->setFlashMessages($this, $messages);

                            // redirect to list view/returnUrl
                            if ($returnUrl) {
                                $this->redirectToUri($returnUrl);
                            } else {
                                $this->redirect('list');
                            }
                        }
                    }

                    // no errors -> add classified
                    if (!count($errors)) {
                        if ($process) {
                            $processed = $this->helperService->processPayment($payment, $messages, $errors);

                            if ($processed['paid']) {

                                // set paid state
                                $item->setPaid(time());

                                // set payment info
                                $item->setPaidInfo(json_encode($processed['response']));

                                // save changes to database
                                $this->classifiedRepository->update($item);
                                $this->persistenceManager->persistAll();

                                // add message
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_payment',
                                                                              $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_payment.success',
                                                                             $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );
                            } else {

                                // set payment info
                                $item->setPaidInfo(json_encode($processed['response']));

                                // save changes to database
                                $this->classifiedRepository->update($item);
                                $this->persistenceManager->persistAll();

                                // add message
                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_payment',
                                                                              $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_payment.error',
                                                                             $this->extensionName
                                    ),
                                    'type' => FlashMessage::ERROR
                                );

                                // set flash messages
                                $this->helperService->setFlashMessages($this, $messages);

                                // redirect to second try
                                $redirectUri = $this->uriBuilder->setNoCache(1)->uriFor('payment',
                                                                                        array('item' => $item->getUid(
                                                                                        ), 'payment' => $payment, 'returnUrl' => $returnUrl),
                                                                                        'Pi1', 'MooxMarketplace', 'Pi1'
                                );
                                $this->redirectToUri($redirectUri);
                            }

                            // set flash messages
                            $this->helperService->setFlashMessages($this, $messages);

                            // redirect to list view/returnUrl
                            if ($returnUrl) {
                                $this->redirectToUri($returnUrl);
                            } else {
                                $this->redirect('list');
                            }

                            $partial = 'Process';
                        } else {
                            $prepared = $this->helperService->preparePayment($payment, $messages, $errors);

                            // connection lost?
                            if (is_array($prepared)) {
                                $prepared['processUrl'] = $this->uriBuilder->setNoCache(true)->setCreateAbsoluteUri(true
                                )->uriFor('payment', array('item' => $item->getUid(
                                ), 'payment' => $payment, 'returnUrl' => $returnUrl, 'process' => 1), 'Pi1',
                                          'MooxMarketplace', 'Pi1'
                                );
                                $partial = 'Prepare';
                            } else {
                                //print_r($prepared);
                                //exit();
                                $this->redirect('error');
                            }
                        }
                    }
                }
            } else {
                if ($item->getPaid() > 0) {
                    $partial = 'Info';
                } else {
                    // set default payment method
                    if (!isset($payment['method'])) {
                        $payment['method'] = current($paymentMethods)['extkey'];
                    }

                    $payment['item'] = $item->getUid();
                }
            }

            // append payment config
            $payment['config'] = $config;
            if ($item->getPaidInfo() != '') {
                $payment['info'] = json_decode($item->getPaidInfo(), true);
            }

            // set flash messages
            $this->helperService->setFlashMessages($this, $messages);

            // set template variables
            $this->view->assign('item', $item);
            $this->view->assign('payment', $payment);
            $this->view->assign('paymentMethods', $paymentMethods);
            $this->view->assign('prepared', $prepared);
            $this->view->assign('processed', $processed);
            $this->view->assign('partial', $partial);
            $this->view->assign('returnUrl', $returnUrl);
            $this->view->assign('action', 'payment');
        }
    }

    /**
     * action edit
     *
     * @param int $item
     * @param array $edit
     * @param string $returnUrl
     * @param bool $reload
     * @return void
     */
    public function editAction($item = null, $edit = null, $returnUrl = null, $reload = false)
    {
        $messages = [];
        $errors = [];
        $fields = [];
        $fileAddOnly = false;
        $editable = false;

        // check and get logged in user
        if ($item > 0) {
            $item = $this->classifiedRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxMarketplace\Domain\Model\Classified')) {
            if ($this->feUser->getUid() == $item->getFeUser()->getUid()) {
                $editable = true;
            }
        }

        // if item is editable by current user
        if (!$editable) {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_edit', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_edit.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );
            $this->helperService->setFlashMessages($this, $messages);
            $this->redirect('error');
        }

        // set variant
        if (isset($edit['variant']) && $edit['variant'] != '') {
            $variant = $edit['variant'];
        } elseif ($item->getVariant() != '') {
            $variant = $item->getVariant();
        } elseif ($this->variant) {
            $variant = $this->variant;
        } else {
            $variant = GeneralUtility::camelCaseToLowerCaseUnderscored($this->allowedVariants[0]);
        }

        // get fields set by plugin
        $fields = $this->helperService->configureFields(
            $this->settings['editFields'],
            $this->settings['requiredEditFields'],
            $this->settings,
            $this->fieldConfig,
            $variant,
            $this->allowedVariants
        );

        // process form values
        if ($edit && !$reload) {
            // add or remove files by add button
            foreach ($fields as $field) {
                // if field is a file field
                if ($field['config']['type'] == 'file') {
                    // if temporary file form fields are set
                    if (is_array($edit[$field['key'] . '_tmp'])) {
                        // go through all temporary file form fields
                        foreach ($edit[$field['key'] . '_tmp'] as $key => $value) {
                            // if remove button for this temporary field is set
                            if (isset($edit[$field['key'] . '_remove_' . $key])) {
                                // prepare get method call
                                $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);
                                // prepare remove method call
                                $removeMethod = 'remove' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                // if get and remove methods are existing
                                if (method_exists($item, $getMethod) && method_exists($item, $removeMethod)) {
                                    // go through all current files
                                    foreach ($item->$getMethod() as $reference) {
                                        // temporary field key equals reference key from object storage
                                        if ($reference->getUid() == $key) {
                                            // remove selected file
                                            $item->$removeMethod($reference);
                                            // save changes to database
                                            $this->classifiedRepository->update($item);
                                            $this->persistenceManager->persistAll();
                                        }
                                    }
                                }

                                $messages[] = array(
                                    'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                    'title' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_edit',
                                        $this->extensionName
                                    ),
                                    'text' => LocalizationUtility::translate(
                                        self::LLPATH . 'pi1.action_all.removed.' . $field['config']['reference-type'],
                                        $this->extensionName
                                    ),
                                    'type' => FlashMessage::OK
                                );
                                $this->helperService->setFlashMessages($this, $messages);
                                $this->redirect('edit', null, null, ['item' => $item]);
                            }
                        }
                    }

                    // if form file add button was pressed for this field
                    if (isset($edit[$field['key']]) && isset($edit[$field['key'] . '_add'])) {

                        // if file is selected in this form file field
                        if ($edit[$field['key']]['name'] != '') {

                            // prepare get method call
                            $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                            // prepare add method call
                            $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                            // if get and add methods are existing
                            if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {

                                // get current count of temporary file form fields for this file field
                                $fileCnt = $item->$getMethod()->count();

                                // if maxitems not reached yet for this field
                                if (!isset($field['config']['maxitems']) || (isset($field['config']['maxitems']) && $fileCnt < $field['config']['maxitems'])) {

                                    // get file storage
                                    $storage = $this->storageRepository->findByUid($this->fileStorageUid);

                                    // create file storage folder if not already existing
                                    if (!$storage->hasFolder($this->fileStorageFolder)) {
                                        $folder = $storage->createFolder($this->fileStorageFolder);
                                    } else {
                                        $folder = $storage->getFolder($this->fileStorageFolder);
                                    }

                                    // add file to storage
                                    $fileObject = $storage->addFile(
                                        $edit[$field['key']]['tmp_name'], $folder, $edit[$field['key']]['name']
                                    );

                                    // get recently added file object from storage
                                    $fileObject = $storage->getFile($fileObject->getIdentifier());

                                    // get real file object from file repo
                                    $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));

                                    // set reference type depending on field
                                    if ($field['config']['reference-type'] == 'image') {
                                        $referenceType = 'ImageReference';
                                    } else {
                                        $referenceType = 'FileReference';
                                    }

                                    // set reference extkey
                                    if ($field['config']['extkeyUCC'] && $field['config']['extkeyUCC'] != 'MooxMarketplace') {
                                        $referenceExtkey = $field['config']['extkeyUCC'];
                                    } else {
                                        $referenceExtkey = 'MooxMarketplace';
                                    }

                                    // get new file reference object to reference the added file
                                    $fileReference = $this->objectManager->get('DCNGmbH\\' . $referenceExtkey . '\Domain\Model\\' . $referenceType
                                    );
                                    $fileReference->setFile($file);
                                    $fileReference->setCruserId(0);

                                    // add new file reference to classified
                                    $item->$addMethod($fileReference);

                                    // save changes to database
                                    $this->classifiedRepository->update($item);
                                    $this->persistenceManager->persistAll();

                                    // add message
                                    $messages[] = array(
                                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                                        'title' => LocalizationUtility::translate(
                                            self::LLPATH . 'pi1.action_edit',
                                            $this->extensionName
                                        ),
                                        'text' => LocalizationUtility::translate(
                                            self::LLPATH . 'pi1.action_all.added.' . $field['config']['reference-type'],
                                            $this->extensionName
                                        ),
                                        'type' => FlashMessage::OK
                                    );

                                    $this->helperService->setFlashMessages($this, $messages);
                                    $this->redirect('edit', null, null, ['item' => $item, 'returnUrl' => $returnUrl]);
                                } else {

                                    // add message
                                    $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.too_many',
                                                                              $this->extensionName,
                                                                              array($field['config']['maxitems'])
                                    );
                                    if ($message == '') {
                                        $message = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                              $field['extkey'],
                                                                                              self::LLPATH
                                                                                  ) . 'form.' . $field['key'] . '.error.too_many',
                                                                                  $field['extkey'],
                                                                                  array($field['config']['maxitems'])
                                        );
                                    }
                                    if ($message == '') {
                                        $message = LocalizationUtility::translate(self::LLPATH . 'form.error.too_many',
                                                                                  $this->extensionName,
                                                                                  array($field['config']['maxitems'])
                                        );
                                    }
                                    $title = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'],
                                                                            $this->extensionName
                                    );
                                    if ($title == '') {
                                        $title = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                            $field['extkey'],
                                                                                            self::LLPATH
                                                                                ) . 'form.' . $field['key'],
                                                                                $field['extkey']
                                        );
                                    }
                                    $messages[] = array(
                                        'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                        'title' => $title,
                                        'text' => $message,
                                        'type' => FlashMessage::ERROR
                                    );

                                    // set error
                                    $errors[$field['key']] = true;
                                }
                            }
                        } else {

                            // add message
                            $message = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'] . '.error.empty',
                                                                      $this->extensionName
                            );
                            if ($message == '') {
                                $message = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                      $field['extkey'], self::LLPATH
                                                                          ) . 'form.' . $field['key'] . '.error.empty',
                                                                          $field['extkey']
                                );
                            }
                            if ($message == '') {
                                $message = LocalizationUtility::translate(self::LLPATH . 'form.error.empty',
                                                                          $this->extensionName
                                );
                            }
                            $title = LocalizationUtility::translate(self::LLPATH . 'form.' . $field['key'],
                                                                    $this->extensionName
                            );
                            if ($title == '') {
                                $title = LocalizationUtility::translate(str_replace('moox_marketplace',
                                                                                    $field['extkey'], self::LLPATH
                                                                        ) . 'form.' . $field['key'], $field['extkey']
                                );
                            }
                            $messages[] = array(
                                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                                'title' => $title,
                                'text' => $message,
                                'type' => FlashMessage::ERROR
                            );

                            // set error
                            $errors[$field['key']] = true;
                        }

                        // set "file add only" to prevent further processing of form values
                        $fileAddOnly = true;
                    }
                }
            }

            // if general save button is pressed
            if (!$fileAddOnly) {

                // check fields
                $this->helperService->checkFields($fields, $edit, $messages, $errors);

                // set item values
                foreach ($fields as $field) {
                    if (!$field['passthrough'] && !in_array($field['config']['type'], array('file', 'tree')
                        ) && !in_array($field['key'], array('variant', 'categories'))
                    ) {
                        $setMethod = 'set' . GeneralUtility::underscoredToUpperCamelCase($field['key']);
                        if (method_exists($item, $setMethod) && isset($edit[$field['key']])) {
                            if (in_array($field['config']['type'], array('date'))) {
                                $item->$setMethod($this->helperService->getTimestamp($edit[$field['key']]));
                            } elseif (in_array($field['config']['type'], array('datetime'))) {
                                $item->$setMethod($this->helperService->getTimestamp($edit[$field['key']],
                                                                                     $edit[$field['key'] . '_hh'],
                                                                                     $edit[$field['key'] . '_mm']
                                )
                                );
                            } else {
                                $item->$setMethod((is_array($edit[$field['key']])) ? implode(',', $edit[$field['key']]
                                ) : trim($edit[$field['key']])
                                );
                            }
                        }
                    }
                }

                // no errors -> save changes
                if (!count($errors)) {

                    // set item variant
                    $item->setVariant($variant);

                    // set file relations
                    foreach ($fields as $field) {

                        // if field is a file field
                        if (in_array($field['config']['type'], array('file'))) {

                            // if file is selected in this form file field
                            if (isset($edit[$field['key']])) {

                                // if file is selected in this form file field
                                if ($edit[$field['key']]['name'] != '') {

                                    // prepare get method call
                                    $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare add method call
                                    $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // if get and add methods are existing
                                    if (method_exists($item, $getMethod) && method_exists($item, $addMethod)) {

                                        // get file storage
                                        $storage = $this->storageRepository->findByUid($this->fileStorageUid);

                                        // create file storage folder if not already existing
                                        if (!$storage->hasFolder($this->fileStorageFolder)) {
                                            $folder = $storage->createFolder($this->fileStorageFolder);
                                        } else {
                                            $folder = $storage->getFolder($this->fileStorageFolder);
                                        }

                                        // add file to storage
                                        $fileObject = $storage->addFile(
                                            $edit[$field['key']]['tmp_name'], $folder, $edit[$field['key']]['name']
                                        );

                                        // get recently added file object from storage
                                        $fileObject = $storage->getFile($fileObject->getIdentifier());

                                        // get real file object from file repo
                                        $file = $this->fileRepository->findByUid($fileObject->getProperty('uid'));

                                        // set reference type depending on field
                                        if ($field['config']['reference-type'] == 'image') {
                                            $referenceType = 'ImageReference';
                                        } else {
                                            $referenceType = 'FileReference';
                                        }

                                        // set reference extkey
                                        if ($field['config']['extkeyUCC'] && $field['config']['extkeyUCC'] != 'MooxMarketplace') {
                                            $referenceExtkey = $field['config']['extkeyUCC'];
                                        } else {
                                            $referenceExtkey = 'MooxMarketplace';
                                        }

                                        // get new file reference object to reference the added file
                                        $fileReference = $this->objectManager->get('DCNGmbH\\' . $referenceExtkey . '\Domain\Model\\' . $referenceType
                                        );
                                        $fileReference->setFile($file);
                                        $fileReference->setCruserId(0);

                                        // add new file reference to classified
                                        $item->$addMethod($fileReference);
                                    }
                                }
                            }

                            // if field is the categories field
                        } elseif ($field['key'] == 'categories') {

                            // if file is selected in this form file field
                            if (isset($edit[$field['key']])) {

                                // if file is selected in this form file field
                                if ($edit[$field['key']] != '') {

                                    // prepare get method call
                                    $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare add method call
                                    $addMethod = 'add' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                                    // prepare remove method call
                                    $removeMethod = 'remove' . GeneralUtility::underscoredToUpperCamelCase($field['key']
                                        );

                                    // if get and add methods are existing
                                    if (method_exists($item, $getMethod) && method_exists($item, $addMethod
                                        ) && method_exists($item, $removeMethod)
                                    ) {

                                        // get selected categories from form
                                        $formItems = explode(',', $edit[$field['key']]);

                                        // remove current elements
                                        if ($item->$getMethod()->count() > 0) {
                                            foreach ($item->$getMethod() as $object) {
                                                $item->$removeMethod($object);
                                            }
                                        }

                                        // add new elements
                                        foreach ($formItems as $formItem) {
                                            $object = $this->categoryRepository->findByUid($formItem);
                                            if (is_object($object)) {
                                                $item->$addMethod($object);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // save item to database
                    $this->classifiedRepository->update($item);
                    $this->persistenceManager->persistAll();

                    // execute process datamap after database operations if available
                    if (class_exists("DCNGmbH\MooxMarketplace\Hooks\Tcemain")) {
                        $Tcemain = $this->objectManager->get('DCNGmbH\\MooxMarketplace\\Hooks\\Tcemain');
                        if (is_object($Tcemain)) {
                            $Tcemain->processDatamap_afterDatabaseOperations(null,
                                                                             'tx_mooxmarketplace_domain_model_classified',
                                                                             (string)$item->getUid(), array(), null
                            );
                        }
                    }

                    // add message
                    $messages[] = array(
                        'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                        'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_edit', $this->extensionName
                        ),
                        'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_edit.success',
                                                                 $this->extensionName
                        ),
                        'type' => FlashMessage::OK
                    );

                    // set flash messages
                    $this->helperService->setFlashMessages($this, $messages);

                    // redirect to edit view
                    $this->redirect('edit', null, null, array('item' => $item, 'returnUrl' => $returnUrl));
                }
            }
        }

        // preset edit files if existing
        if (!$edit && !$reload) {

            // go trough all registered fields
            foreach ($fields as $field) {

                // prepare get method call
                $getMethod = 'get' . GeneralUtility::underscoredToUpperCamelCase($field['key']);

                // if get method exists
                if (method_exists($item, $getMethod)) {

                    // if field is a file field
                    if (in_array($field['config']['type'], array('file'))) {

                        // go through all existing files of this field
                        foreach ($item->$getMethod() as $fileReference) {

                            // get file reference
                            $fileReference = $fileReference->getOriginalResource();

                            // get real file
                            $file = $fileReference->getOriginalFile();

                            // get current count of temporary file form fields for this file field
                            $fileCnt = (is_array($edit[$field['key'] . '_tmp'])) ? count($edit[$field['key'] . '_tmp']
                            ) : 0;

                            // set temporary file form fields for existing file
                            $edit[$field['key'] . '_tmp'][$fileCnt]['name'] = $file->getName();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['src'] = $file->getPublicUrl();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['ext'] = $fileReference->getExtension();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['size'] = $fileReference->getSize();
                            $edit[$field['key'] . '_tmp'][$fileCnt]['reference'] = $fileReference;
                        }

                        // if field is a tree field
                    } elseif (in_array($field['config']['type'], array('tree'))) {

                        // init tree
                        $treeItems = [];

                        // go through all existing tree items
                        foreach ($item->$getMethod() as $treeItem) {

                            // add to tree
                            $treeItems[] = $treeItem->getUid();
                        }

                        // set temporary file form field for existing tree items
                        $edit[$field['key']] = implode(',', $treeItems);
                    } elseif (in_array($field['config']['type'], array('check'))) {

                        // preset form field
                        $edit[$field['key']] = ($item->$getMethod() != '') ? explode(',', $item->$getMethod()) : '';
                    } elseif (in_array($field['config']['type'], array('date')) && $item->$getMethod() != '') {
                        $edit[$field['key']] = date('d.m.Y', $item->$getMethod());
                    } elseif (in_array($field['config']['type'], array('datetime')) && $item->$getMethod() != '') {
                        $edit[$field['key']] = date('d.m.Y', $item->$getMethod());
                        $edit[$field['key'] . '_hh'] = date('H', $item->$getMethod());
                        $edit[$field['key'] . '_mm'] = date('i', $item->$getMethod());
                    } elseif (!$field['passthrough']) {

                        // preset form field
                        $edit[$field['key']] = $item->$getMethod();
                    }
                }
            }
        }

        // set current variant
        if (!isset($edit['variant'])) {
            $edit['variant'] = $variant;
        }

        // set flash messages
        $this->helperService->setFlashMessages($this, $messages);

        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('edit', $edit);
        $this->view->assign('fields', $fields);
        $this->view->assign('errors', $errors);
        $this->view->assign('returnUrl', $returnUrl);
        $this->view->assign('action', 'edit');
    }

    /**
     * action delete
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function deleteAction($item = null, $returnUrl = null)
    {

        // init action arrays and booleans
        $messages = [];
        $deleteAble = false;

        // check and get logged in user
        if ($item > 0) {
            $item = $this->classifiedRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxMarketplace\Domain\Model\Classified')) {
            if ($this->feUser->getUid() == $item->getFeUser()->getUid()) {
                $deleteAble = true;
            }
        }

        // if item is editable by current user
        if ($deleteAble) {
            $this->classifiedRepository->remove($item);
            $this->persistenceManager->persistAll();

            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_delete', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_delete.success',
                    $this->extensionName
                ),
                'type' => FlashMessage::OK
            );
            $this->helperService->setFlashMessages($this, $messages);

            if ($returnUrl) {
                $this->redirectToUri($returnUrl);
            } else {
                $this->redirect('list');
            }
        } else {
            $messages[] = array(
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_delete', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_delete.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            );
            $this->helperService->setFlashMessages($this, $messages);
            $this->redirect('error');
        }

        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('action', 'delete');
    }

    /**
     * action visibility
     *
     * @param int $item
     * @param string $returnUrl
     * @return void
     */
    public function visibilityAction($item = null, $returnUrl = null)
    {
        $messages = [];
        $editable = false;

        // check and get logged in user
        if ($item > 0) {
            $item = $this->classifiedRepository->findByUid($item, false);
        }

        // if user is logged in and correct item is passed
        if ($this->feUser && is_a($item, '\DCNGmbH\MooxMarketplace\Domain\Model\Classified')) {
            if ($this->feUser->getUid() == $item->getFeUser()->getUid()) {
                $editable = true;
            }
        }

        // if item is editable by current user
        if (!$editable) {
            $messages[] = [
                'icon' => '<span class="glyphicon glyphicon-warning-sign icon-alert" aria-hidden="true"></span>',
                'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_visibility', $this->extensionName),
                'text' => LocalizationUtility::translate(
                    self::LLPATH . 'pi1.action_visibility.error.no_access',
                    $this->extensionName
                ),
                'type' => FlashMessage::ERROR
            ];
            $this->helperService->setFlashMessages($this, $messages);
            $this->redirect('error');
        }

        // change visibility
        if ($item->getHidden() == 1) {
            $item->setHidden(0);
        } else {
            $item->setHidden(1);
        }

        // remove item from database
        $this->classifiedRepository->update($item);
        $this->persistenceManager->persistAll();

        // add message
        $messages[] = array(
            'icon' => '<span class="glyphicon glyphicon-ok icon-alert" aria-hidden="true"></span>',
            'title' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_visibility', $this->extensionName),
            'text' => LocalizationUtility::translate(self::LLPATH . 'pi1.action_visibility.success',
                                                     $this->extensionName
            ),
            'type' => FlashMessage::OK
        );

        // set flash messages
        $this->helperService->setFlashMessages($this, $messages);

        // redirect to list view
        if ($returnUrl) {
            $this->redirectToUri($returnUrl);
        } else {
            $this->redirect('list');
        }

        // set template variables
        $this->view->assign('item', $item);
        $this->view->assign('action', 'visibility');
    }

    /**
     * action error
     *
     * @return void
     */
    public function errorAction()
    {
        $this->view->assign('action', 'error');
    }
}
