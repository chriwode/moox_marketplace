<?php
namespace DCNGmbH\MooxMarketplace\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility; 
 
/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class TcaFormHelper {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $objectManager;
	
	/**
	 * helperService
	 *
	 * @var \DCNGmbH\MooxMarketplace\Service\HelperService	
	 */
	protected $helperService;
	
	/**
	 * paymentService
	 *
	 * @var \DCNGmbH\MooxPayment\Service\PaymentService	
	 */
	protected $paymentService;
	
	/**
	 * extConf
	 *
	 * @var \array	
	 */
	protected $extConf;
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang_be.xlf:';
	
	/**
     * initialize action
	 *
     * @return void
     */
    public function initialize() {					
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
		
		// init helper service
		$this->helperService = $this->objectManager->get('DCNGmbH\\MooxMarketplace\\Service\\HelperService');

		// init payment service
		if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_payment')){
			$this->paymentService = $this->objectManager->get('DCNGmbH\MooxPayment\Service\PaymentService');
		}
		
		// get extensions's configuration
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace']);
    }
	
	/**
	 * Modifies the select box of classified-variants-options
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function classifiedVariants(array &$config) {
		
		// initialize
		$this->initialize();
		
		foreach($GLOBALS['TCA']['tx_mooxmarketplace_domain_model_classified']['columns']['variant']['config']['items'] AS $option){
			$config['items'][] = array($GLOBALS['LANG']->sL($option[0], TRUE),$option[1]);			
		}		
	}
	
	/**
	 * Modifies the select box of payment-method-options
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function paymentMethods(array &$config) {
		
		// initialize
		$this->initialize();		
		
		if(is_object($this->paymentService)){			
			$paymentMethods = $this->paymentService->getAllPaymentMethods();
			if(count($paymentMethods)){
				foreach($paymentMethods AS $method => $methodConfig){
					// add item
					$config['items'][] = array($methodConfig['title'],$method);									
				}
			} else {
				// add item
				$config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH."pi1.payment_methods.not_found", TRUE),"");
			}
		} else {
			// add item
			$config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH."pi1.payment_methods.not_installed", TRUE),"");
		}	
	}
	
	/**
	 * generate tree selection form element
	 *
	 * @param array $PA
	 * @param array $fobj
	 * @return string $tcaForm
	 */
	function treeSelector(&$PA, &$fobj)    {
		
		// initialize
		$this->initialize();
		$categoryRepository = $this->objectManager->get('DCNGmbH\MooxMarketplace\Domain\Repository\CategoryRepository');
		
		$options 	= array();
		$uids 		= array();		
		$values 	= explode(",",$PA['itemFormElValue']);
		
		foreach($values AS $value){
			$value 		= explode("|",$value);
			if($value[1]!=""){
				$options[] 	= array("value" => $value[0],"label" => $value[1]);
				$uids[] 	= $value[0]; 			
			}
		}
		
		$variant = $PA['row']['variant'];
		if(!$variant){
			$variant = $GLOBALS['TCA']['tx_mooxmarketplace_domain_model_classified']['columns']['variant']['config']['items'][0][1];
		}
		
		if($this->extConf['categoryRestriction']=="current_pid"){
			$categories = $this->helperService->makeTree($categoryRepository->findByVariantsAndStoragePage($PA['row']['pid'],array($variant)));
		} else {
			$categories = $this->helperService->makeTree($categoryRepository->findByVariantsAndStoragePage(NULL,array($variant)));
		}
		
		$fobj->additionalCode_pre[] = '
				<link rel="stylesheet" type="text/css" href="'.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_marketplace').'Resources/Public/Css/Backend/tca_tree_selector.css" />';
		$fobj->additionalCode_pre[] = '
				<script src="'.\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_marketplace').'Resources/Public/JavaScript/Backend/tca_tree_selector.js" type="text/javascript"></script>';
		
		$code .= '<div class="moox-marketplace-tca">';
		$code .= '<div class="moox-marketplace-tca-tree-selector-display-wrapper">';
		$code .= '<div class="moox-marketplace-tca-tree-selector-display-header">Kategorie-Sortierung</div>';
		$code .= '<select id="'.md5($PA['itemFormElName']).'_list" size="10" class="formField tceforms-multiselect moox-marketplace-tca-tree-selector-display" multiple="multiple" name="'.$PA['itemFormElName'].'_list"';      
        $code .= ' onchange="'.htmlspecialchars(implode('', $PA['fieldChangeFunc'])).'"';
        $code .= $PA['onFocus'];
        $code .= '>';
		$optionCnt = 1;
		foreach($options AS $option){
			if($optionCnt==1){
				$code .= '<option value="'.$option['value'].'">'.self::getCategoryLabel($option['value']).' (Hauptkategorie)</option>';
			} else {
				$code .= '<option value="'.$option['value'].'">'.self::getCategoryLabel($option['value']).'</option>';
			}
			$optionCnt++;
		}
		$code .= '</select>';
		$code .= '<input type="hidden" id="moox-marketplace-tca-tree-selector-hidden" name="'.$PA['itemFormElName'].'" value="'.implode(",",$uids).'">';
		$code .= '<input type="hidden" id="moox-marketplace-tca-tree-selector-max" name="moox-marketplace-tca-tree-selector-max" value="'.$PA['fieldConf']['config']['maxitems'].'">';
		if($PA['fieldConf']['config']['minitems']>0){
			$code .= '<input type="hidden" id="moox-marketplace-tca-tree-selector-min" name="moox-marketplace-tca-tree-selector-min" value="'.$PA['fieldConf']['config']['minitems'].'">';
		}
		$code .= '<div class="moox-marketplace-tca-tree-selector-display-footer">';
		if($PA['fieldConf']['config']['maxitems']>0){
			$code .= '(<span id="moox-marketplace-tca-tree-selector-cnt">'.count($options).'</span> von '.(($PA['fieldConf']['config']['maxitems']!=9999)?$PA['fieldConf']['config']['maxitems']:"unbegrenzt").' möglichen Kategorien gewählt)';
		}
		$code .= '</div>';
		$code .= '</div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-actions">';
		$code .= '<div class="moox-marketplace-tca-tree-selector-actions-icon"><span onclick="mooxMarketplaceMoveOptionsToTop(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-to-top t3-btn t3-btn-moveoption-top" title="Ausgewählte Objekte zum Anfang verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-actions-icon"><span onclick="mooxMarketplaceMoveOptionsOneUp(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-up t3-btn t3-btn-moveoption-up" title="Ausgewählte Objekte nach oben verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-actions-icon"><span onclick="mooxMarketplaceMoveOptionsOneDown(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-down t3-btn t3-btn-moveoption-down" title="Ausgewählte Objekte nach unten verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-actions-icon"><span onclick="mooxMarketplaceMoveOptionsToBottom(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-move t3-icon-move-to-bottom t3-btn t3-btn-moveoption-bottom" title="Ausgewählte Objekte zum Ende verschieben">&nbsp;</span></div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-actions-icon"><span onclick="mooxMarketplaceRemoveOptions(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-actions t3-icon-actions-selection t3-icon-selection-delete t3-btn t3-btn-removeoption" title="Ausgewähltes Objekt löschen">&nbsp;</span></div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-actions-icon"><span onclick="mooxMarketplaceSetMainCategory(\'#'.md5($PA['itemFormElName']).'\')" class="t3-icon t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut" title="Ausgewähltes Objekt löschen">&nbsp;</span></div>';
		$code .= '</div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-selector-wrapper">';
		$code .= '<div class="moox-marketplace-tca-tree-selector-selector-header">Kategorie-Auswahl</div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-selector">';		
		$code .= self::generateCategorySelectionTree(md5($PA['itemFormElName']),$variant,$categories,$uids);	
		$code .= '</div>';
		$code .= '<div class="moox-marketplace-tca-tree-selector-selector-footer">';
		$code .= '<span onclick="mooxMarketplaceAddAllOptions(\'#'.md5($PA['itemFormElName']).'\')" class="moox-marketplace-icon icon-select-all">Alle auswählen</span>';
		$code .= '<span onclick="mooxMarketplaceRemoveAllOptions(\'#'.md5($PA['itemFormElName']).'\')" class="moox-marketplace-icon icon-unselect-all">Alle abwählen</span>';
		$code .= '</div>';
		$code .= '</div>';
		$code .= '</div>';		
        
		return $code;		
	}
	
	/**
	 * generate category selection tree
	 *
	 * @param string $id identifier of selection field
	 * @param string $variant variant of selection field
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $categories categories
	 * @param array $selected selected categories
	 * @param string $class class
	 * @param integer $depth depth
	 * @return string $tree
	 */
	function generateCategorySelectionTree($id,$variant,$categories,$selected = array(),$class="",$title="",$depth = 0){
		$tree  = "<ul>";
		foreach($categories AS $category){
			if($category['item']->getClassifiedVariants()=='' || $variant=='' || in_array($variant,explode(",",$category['item']->getClassifiedVariants()))){			
				if(in_array($category['item']->getUid(),$selected)){
					$activeClass = " moox-marketplace-tca-tree-selector-active"; 
				} else {
					$activeClass = "";
				}
				if($class!=""){
					$elementClass = $class." moox-marketplace-tca-tree-selector-".$category['item']->getUid();
				} else {
					$elementClass = " moox-marketplace-tca-tree-selector moox-marketplace-tca-tree-selector-".$category['item']->getUid();
				}
				if($title!=""){
					$wrapperTitle = $title." &rsaquo; ".$category['item']->getTitle();
				} else {
					$wrapperTitle = $category['item']->getTitle();
				}
				$wrapperClass = str_replace("-category","-category-wrapper",$elementClass);
				if(isset($category['children'])){
					$isParentClass = " is-parent";
				} else {
					$isParentClass = "";
				}
				$tree .= '<li id="moox-marketplace-tca-tree-selector-'.$category['item']->getUid().'" class="'.$elementClass.'">';
				$tree .= '<div id="moox-marketplace-tca-tree-selector-wrapper-'.$category['item']->getUid().'" title="'.$category['item']->getTitle().' [UID: '.$category['item']->getUid().']" onclick="mooxMarketplaceToggleOption(\'#'.$id.'\','.$category['item']->getUid().',\''.$wrapperTitle.'\')" class="moox-marketplace-tca-tree-selector-wrapper'.$wrapperClass.$isParentClass.$activeClass.'">';			
				if($selected[0]==$category['item']->getUid()){
					$tree .= '<span id="moox-marketplace-tca-tree-selector-icon-'.$category['item']->getUid().'" class="t3-icon t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-marketplace-tca-tree-selector-icon">&nbsp;</span>';
				} else {
					$tree .= '<span id="moox-marketplace-tca-tree-selector-icon-'.$category['item']->getUid().'" class="t3-icon t3-icon-mimetypes t3-icon-mimetypes-x t3-icon-x-sys_category moox-marketplace-tca-tree-selector-icon">&nbsp;</span>';
				}
				$tree .= '<span id="moox-marketplace-tca-tree-selector-title-'.$category['item']->getUid().'" title="'.$wrapperTitle.'" class="moox-marketplace-tca-tree-selector-title">'.$category['item']->getTitle().'</span>';
				$tree .= '</div>';			
				$tree .= '<div id="moox-marketplace-tca-tree-selector-context-'.$category['item']->getUid().'" class="moox-marketplace-tca-tree-selector-context">';
				$tree .= '<div onclick="mooxMarketplaceSetMainCategory(\'#'.$id.'\','.$category['item']->getUid().',\''.$wrapperTitle.'\')" class="moox-marketplace-context-element icon-select-maincategory"><span class="t3-icon t3-icon-apps t3-icon-apps-toolbar t3-icon-toolbar-menu-shortcut moox-marketplace-tca-tree-selector-icon">&nbsp;</span>"'.$category['item']->getTitle().'" als Hauptkategorie wählen</div>';
				if($isParentClass){
					$tree .= '<div onclick="mooxMarketplaceAddAllBranchOptions(\'#'.$id.'\','.$category['item']->getUid().')" class="moox-marketplace-context-element icon-select-branch">Kompletten Bereich "'.$category['item']->getTitle().'" auswählen</div>';
					$tree .= '<div onclick="mooxMarketplaceRemoveAllBranchOptions(\'#'.$id.'\','.$category['item']->getUid().')" class="moox-marketplace-context-element icon-unselect-branch">Kompletten Bereich "'.$category['item']->getTitle().'" abwählen</div>';
				}
				$tree .= '</div>';	
				
				if(isset($category['children'])){
					$tree .= self::generateCategorySelectionTree($id,$variant,$category['children'],$selected,$elementClass,$wrapperTitle,$depth+1)."</li>";				
				} else {
					$tree .= "</li>";
				}
			}
		}
		$tree .= "</ul>";
		return $tree;
	}
	
	/**
	 * get category label
	 *
	 * @param integer $uid category uid	 
	 * @return string $label
	 */
	function getCategoryLabel($uid = 0)    {
				
		// initialize
		$this->initialize();
		$categoryRepository = $this->objectManager->get('DCNGmbH\MooxMarketplace\Domain\Repository\CategoryRepository');
		
		$uids = array($uid);
		
		while($uid>0){		
			$parent = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
				'parent',
				'sys_category',
				'uid='.$uid,
				'',
				'',
				1
			);
			if($parent[0]['parent']>0){
				$uids[] = $parent[0]['parent'];
			}
			$uid = $parent[0]['parent'];
		}
		
		$uids = array_reverse($uids);
		
		$category 	= $categoryRepository->findByUid($uids[0]);
		if(is_object($category)){
			$label 		= $category->getTitle();
		}
		unset($uids[0]);
		foreach($uids AS $uid){
			$category = $categoryRepository->findByUid($uid);
			if(is_object($category)){
				$label = $label." &rsaquo; ".$category->getTitle();
			}		
		}
		
		return $label;		
	}

}