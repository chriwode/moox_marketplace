<?php
namespace DCNGmbH\MooxMarketplace\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility; 
 
/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tcemain {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $objectManager;
	
	/**
	 * classifiedRepository
	 *
	 * @var \DCNGmbH\MooxMarketplace\Domain\Repository\ClassifiedRepository	 
	 */
	protected $classifiedRepository;
	
	/**
	 * persistenceManager
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface	 
	 */
	protected $persistenceManager;
	
	/**
	 * extConf
	 *
	 * @var \array	
	 */
	protected $extConf;
	
	/**
     * initialize action
	 *
     * @return void
     */
    public function initialize() {					
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');		
		
		// init classified repository
		$this->classifiedRepository = $this->objectManager->get('DCNGmbH\MooxMarketplace\Domain\Repository\ClassifiedRepository');
		
		// init persistence manager
		$this->persistenceManager = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface');

		// get extensions's configuration
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace']);
    }
	
	/**
	 * Flushes the cache if a news record was edited.
	 *
	 * @param array $params
	 * @return void
	 */
	public function clearCachePostProc(array $params) {
		if (isset($params['table']) && $params['table'] === 'tx_mooxmarketplace_domain_model_classified' && isset($params['uid'])) {
			$cacheTag = $params['table'] . '_' . $params['uid'];

			/** @var $cacheManager \TYPO3\CMS\Core\Cache\CacheManager */
			$cacheManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager');
			$cacheManager->getCache('cache_pages')->flushByTag($cacheTag);
			$cacheManager->getCache('cache_pagesection')->flushByTag($cacheTag);
			$cacheManager->getCache('cache_pages')->flushByTag('tx_mooxmarketplace');
			$cacheManager->getCache('cache_pagesection')->flushByTag('tx_mooxmarketplace');
		}
	}

	/**
	 * Generate a different preview link     *
	 * @param string $status status
	 * @param string $table table name
	 * @param integer $recordUid id of the record
	 * @param array $fields fieldArray
	 * @param NULL|\TYPO3\CMS\Core\DataHandling\DataHandler $parentObject parent Object
	 * @return void
	 */
	public function processDatamap_afterDatabaseOperations($status, $table, $recordUid, array $fields, \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject = NULL) {
		
		if ($table === 'tx_mooxmarketplace_domain_model_classified') {
			
			// initialize
			$this->initialize();
			
			$oldRecordUid = $recordUid;
			
			if (!is_numeric($recordUid)) {
				if(!is_object($parentObject)){
					$recordUid = $parentObject->substNEWwithIDs[$recordUid];
				}
			}
			
			if($recordUid){
				
				$doUpdate = false;
								
				$item = $this->classifiedRepository->findByUid((int)$recordUid,false);
						
				if(is_object($item)){
					
					// get post save functions from extender extensions
					$extendedFunctions = array();
					$functions = get_class_methods(get_class($this));
					foreach($functions AS $function){
						if(substr($function,0,9)=="extended_"){
							$extendedFunctions[] = $function;
						}
					}
					
					foreach($extendedFunctions AS $extendedFunction){
						if($this->$extendedFunction($item)){
							$doUpdate = true;
						}
					}
					
					if($doUpdate){
						$this->classifiedRepository->update($item);	
						$this->persistenceManager->persistAll();				
					}
				}				
			}								
		}
	}
}