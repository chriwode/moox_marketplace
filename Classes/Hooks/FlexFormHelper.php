<?php
namespace DCNGmbH\MooxMarketplace\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
 
/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FlexFormHelper {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $objectManager;
	
	/**
	 * configurationManager
	 *
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface	
	 */
	protected $configurationManager;
	
	/**
	 * flexFormService
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager	
	 */
	protected $flexFormService;
	
	/**
	 * paymentService
	 *
	 * @var \DCNGmbH\MooxPayment\Service\PaymentService	
	 */
	protected $paymentService;
	
	/**
	 * helperService
	 *
	 * @var \DCNGmbH\MooxMarketplace\Service\HelperService	
	 */
	protected $helperService;
	
	/**
	 * pageRepository
	 *
	 * @var \TYPO3\CMS\Frontend\Page\PageRepository	
	 */
	protected $pageRepository;
	
	/**
	 * contentRepository
	 *
	 * @var \DCNGmbH\MooxMarketplace\Domain\Repository\ContentRepository
	 */
	protected $contentRepository;		
	
	/**
	 * frontendUserGroupRepository
	 *
	 * @var \DCNGmbH\MooxMarketplace\Domain\Repository\FrontendUserGroupRepository
	 */
	protected $frontendUserGroupRepository;
	
	/**
	 * configuration
	 *
	 * @var \array	
	 */
	protected $configuration;
	
	/**
	 * extConf
	 *
	 * @var \array	
	 */
	protected $extConf;
	
	/**
	 * Path to the locallang file
	 * @var string
	 */
	const LLPATH = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang_be.xlf:';
	
	/**
     * initialize action
	 *
     * @return void
     */
    public function initialize() {					
		
		// initialize object manager
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		
		// initialize configuration manager
		$this->configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
		
		// initialize flex form service
		$this->flexFormService = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Service\\FlexFormService');
		
		// init helper service
		$this->helperService = $this->objectManager->get('DCNGmbH\\MooxMarketplace\\Service\\HelperService');
		
		// init payment service
		if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_payment')){
			$this->paymentService = $this->objectManager->get('DCNGmbH\MooxPayment\Service\PaymentService');
		}
		
		// initialize page repository
		$this->pageRepository = $this->objectManager->get('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
		
		// initialize content repository
		$this->contentRepository = $this->objectManager->get('DCNGmbH\\MooxMarketplace\\Domain\\Repository\\ContentRepository');
		
		// initialize frontend user group repository
		$this->frontendUserGroupRepository = $this->objectManager->get('DCNGmbH\\MooxCommunity\\Domain\\Repository\\FrontendUserGroupRepository');		
		
		// get typoscript configuration
		$this->configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,"MooxMarketplace");
		
		// get extensions's configuration
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace']);
    }
	
	/**
	 * Itemsproc function to generate the selection of switchable controller actions
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function switchableControllerActions(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// define action list for each switchable controller actions element
		$actionsManageList = array("list","detail","add","edit","delete","visibility","payment","error");		
		
		// set actions array for "manage"
		$actionsManage = array();
		foreach($actionsManageList AS $actionManage){
			$actionsManage[] = "Pi1->".$actionManage;
		}
		
		// set label for "manage"
		$labelManage = $GLOBALS['LANG']->sL(self::LLPATH.'pi1.selection.my_classifieds');		
		
		// add items
		$config['items'][] = array(0 => $labelManage, 1 => implode(";",$actionsManage));
		
		// set default item
		$config['config']['default'] = implode(";",$actionsManage);		
	}
	
	/**
	 * Itemsproc function to extend the selection of storage pid in flexform
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function storagePid(array &$config, &$pObj) {
		
		// initialize
		$this->initialize();		
		
		// if valid persistence storage pid is set within typoscript setup
		if($this->configuration['persistence']['storagePid']!=""){						
			
			// get page info for storage pid set by typoscript
			$page = $this->pageRepository->getPage($configuration['persistence']['storagePid']);
			$definedByTs = array(array("[Defined by TS]: ".$page['title']." [PID: ".$this->configuration['persistence']['storagePid']."]","TS"));
		} 
		
		// add pid postfix to item array element and remove invalid pids from array
		for($i=0;$i<count($config['items']);$i++){
			if($config['items'][$i][1]!=""){
				$config['items'][$i][0] = $config['items'][$i][0]." [PID: ".$config['items'][$i][1]."]";
			} 			
		}
		
		// if available add defined by ts value to items array
		if($definedByTsTxt){
			$config['items'] = array_merge($definedByTs,$config['items']);
		}
	}		
	
	/**
	 * Itemsproc function to process the selection of fe groups in flexform
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function feGroups(array &$config, &$pObj) {
		
		// add pid postfix to item array element and remove invalid pids from array
		for($i=0;$i<count($config['items']);$i++){
			if($config['items'][$i][1]!=""){
				$config['items'][$i][0] = $config['items'][$i][0]." [PID: ".$config['items'][$i][1]."]";
			} 			
		}
	}
	
	/**
	 * Itemsproc function to provide a selection of available payment methods
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function paymentMethods(array &$config, &$pObj) {
		
		// initialize
		$this->initialize();		
		
		// init items array
		$config['items'] = array();
		
		if(is_object($this->paymentService)){			
			$paymentMethods = $this->paymentService->getAllPaymentMethods();
			if(count($paymentMethods)){
				foreach($paymentMethods AS $method => $methodConfig){
					// add item
					$config['items'][] = array($methodConfig['title'],$method);									
				}
			} else {
				// add item
				$config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH."pi1.payment_methods.not_found", TRUE),"");
			}
		} else {
			// add item
			$config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH."pi1.payment_methods.not_installed", TRUE),"");
		}		
	}
	
	/**
	 * Itemsproc function to get a selection of available variants
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function variants(array &$config) {
		
		// initialize
		$this->initialize();
		
		// init items array
		$config['items'] = array();
		
		// add item
		$config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH."pi1.variant.userdefined", TRUE),"");
		
		// get all variants
		foreach($this->helperService->getAvailableVariants() AS $option){
			$config['items'][] = array($option[0],$option[1]);			
		}		
	}
	
	/**
	 * Itemsproc function to get a selection of allowed variants
	 *
	 * @param array &$config configuration array
	 * @return void
	 */
	public function allowedVariants(array &$config) {
		
		// initialize
		$this->initialize();
		
		// init items array
		$config['items'] = array();
		
		// get all variants
		foreach($this->helperService->getAvailableVariants() AS $option){
			$config['items'][] = array($option[0],$option[1]);			
		}		
	}
	
	/**
	 * Itemsproc function to generate list of available fields
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @param string $action action
	 * @param array $excludeFields exclude fields
	 * @param boolean $excludeHeader exclude header fields
	 * @return void
	 */
	public function fields(array &$config, &$pObj, $action = "", $excludeFields = array(), $excludeHeader = true) {
		
		// initialize
		$this->initialize();
		
		// set local language path
		$llpath = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang.xlf:';
		
		// init items array
		$items = array();
						
		// get classified dummy
		$classified = $this->objectManager->get('DCNGmbH\\MooxMarketplace\\Domain\\Model\\Classified');		
		
		// filter sort fields only
		if($action=="sort"){
			$action = "list";
			$sortable = 1;
		}
		
		// get plugin fields
		$pluginFields = $this->helperService->getPluginFields("tx_mooxmarketplace_domain_model_classified","mooxmarketplace",$action);		
		
		// set translation lookup array
		if(in_array($action,array("add","edit"))){
			$lookup = "form";
		} else {
			$lookup = $action;
		}
		
		// get flex form data array
		if(in_array($action,array("add","edit"))){
			$flexformData = $this->flexFormService->convertFlexFormContentToArray($config['row']['pi_flexform']);				
			if($flexformData['settings']['variant']!=""){
				$excludeFields[] = "variant";
				$variant = $flexformData['settings']['variant'];
			}
		}
		
		// add valid fields to items array
		foreach($pluginFields AS $fieldname => $field){
						
			if((!$excludeHeader || !$field['moox']['header']) && !in_array($fieldname,$excludeFields) && (!$sortable || $field['moox']['sortable']) && ($variant=="" || $field['moox']['extkey']=="moox_marketplace" || $field['moox']['extkey']==$variant || $field['moox']['variant']==$variant)){				
				// check if field has valid setter/getter method
				$setMethod = "set".GeneralUtility::underscoredToUpperCamelCase($fieldname);
				$getMethod = "get".GeneralUtility::underscoredToUpperCamelCase($fieldname);
				if((in_array($action,array("add","edit")) && method_exists($classified,$setMethod)) || (in_array($action,array("list","detail")) && method_exists($classified,$getMethod)) || (!$excludeHeader && $field['moox']['header'])){
					$label = $GLOBALS['LANG']->sL($llpath.$lookup.'.'.$fieldname);
					if($label==""){
						$label = $GLOBALS['LANG']->sL($llpath.'form.'.$fieldname);
					}
					if($label==""){
						$label = $GLOBALS['LANG']->sL($field['label']);
					}
					if($label==""){
						$label = $fieldname;
					}
					if($field['moox']['header']){
						$label = '['.$GLOBALS['LANG']->sL($llpath.'header').'] '.$label;
					} elseif($field['moox']['extkey']!="moox_marketplace"){
						$label = '['.$field['moox']['extkey'].'] '.$label;
					}
					$items[] = array(0 => $label, 1 => $fieldname);
				}
			}
		}
		
		return $items;
	}
	
	/**
	 * Itemsproc function to generate list of available list fields
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function listFields(array &$config, &$pObj) {
		
		// set local language path
		$llpath = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang.xlf:';
		
		// init items array
		$config['items'] = array();
		
		// get lisr fields
		$config['items'] = $this->fields($config,$pObj,"list");
		$config['items'][] = array($GLOBALS['LANG']->sL($llpath.'list.summary'),"summary");
	}
	
	/**
	 * Itemsproc function to generate list of available detail fields
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function detailFields(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// get detail fields
		$config['items'] = $this->fields($config,$pObj,"detail");		
	}
	
	/**
	 * Itemsproc function to generate list of available add fields
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function addFields(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// get add fields
		$config['items'] = $this->fields($config,$pObj,"add",array(),false);				
	}
	
	/**
	 * Itemsproc function to generate list of available edit fields
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function editFields(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// get edit fields
		$config['items'] = $this->fields($config,$pObj,"edit",array(),false);	
	}		
	
	/**
	 * Itemsproc function to generate list of available add fields (required)
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function requiredAddFields(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// get required add fields
		$config['items'] = $this->fields($config,$pObj,"add");		
	}
	
	/**
	 * Itemsproc function to generate list of available edit fields (required)
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function requiredEditFields(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// get required edit fields
		$config['items'] = $this->fields($config,$pObj,"edit");		
	}	

	/**
	 * Itemsproc function to generate list of available order fields
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function orderBy(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// add item
		$config['items'][] = array($GLOBALS['LANG']->sL(self::LLPATH."pi1.order_by.userdefined", TRUE),"");
		
		// get sort fields from list fields
		foreach($this->fields($config,$pObj,"sort") AS $option){
			$config['items'][] = array($option[0],$option[1]);
		}
	}
	
	/**
	 * Itemsproc function to generate list of allowed order fields
	 *
	 * @param array &$config configuration array
	 * @param mixed &$pObj configuration array
	 * @return void
	 */
	public function allowedOrderBy(array &$config, &$pObj) {
		
		// init items array
		$config['items'] = array();
		
		// get sort fields from list fields
		$config['items'] = $this->fields($config,$pObj,"sort");
	}
}