<?php
namespace DCNGmbH\MooxMarketplace\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser;

/**
 *
 *
 * @package moox_marketplace
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AccessControlService implements \TYPO3\CMS\Core\SingletonInterface {
	
	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	protected $objectManager;	
	
	/**
	 * frontendUserRepository
	 *
	 * @var \DCNGmbH\MooxMarketplace\Domain\Repository\FrontendUserRepository
	 * @inject
	 */
	protected $frontendUserRepository;
	
	/**
     * Do we have a logged in feuser
     *
	 * @return boolean
     */
    public function hasLoggedInFrontendUser() {		
        return ($GLOBALS['TSFE']->loginUser == 1) ? TRUE : FALSE;
    }
 
    /**
     * Get the uid of the current feuser
     *
	 * @return mixed
     */
    public function getFrontendUserUid() {
        if ($this->hasLoggedInFrontendUser() && !empty($GLOBALS['TSFE']->fe_user->user['uid'])) {
            return intval($GLOBALS['TSFE']->fe_user->user['uid']);
        }
        return NULL;
    }
	
	/**
     * check if adding items is allowed without payment
     *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser
	 * @param \array $settings
	 * @return \boolean
     */
    public function addAllowedWithoutPayment(\DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser $feUser = NULL, $settings = array()) {
        
		$allowed = false;
		
		if($settings['feGroupsFree']!=""){
			$allowedUsergroups = explode(",",$settings['feGroupsFree']);
			$usergoups = array();
			foreach($feUser->getUsergroup() AS $group){
				if(in_array($group->getUid(),$allowedUsergroups)){
					$allowed = true;
					break;
				}
			}
		}
		
		return $allowed;
    }
	
	/**
     * check if adding items is allowed with payment
     *
	 * @param \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser
	 * @param \array $settings
	 * @return \boolean
     */
    public function addAllowedWithPayment(\DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser $feUser = NULL, $settings = array()) {
        
		$allowed = false;
		
		if($settings['feGroupsPaid']!=""){
			$allowedUsergroups = explode(",",$settings['feGroupsPaid']);
			$usergoups = array();
			foreach($feUser->getUsergroup() AS $group){
				if(in_array($group->getUid(),$allowedUsergroups)){
					$allowed = true;
					break;
				}
			}
		}
		
		return $allowed;
    }

    /**
     * Control if the user could use their inclusive quota or if her must payed
     *
     * @param \DCNGmbH\MooxMarketplace\Domain\Model\FrontendUser $feUser
     * @param string $newEntryType
     * @return bool Return true if no payment need
     */
    public function checkPaymentStatus(FrontendUser $feUser, $newEntryType)
    {
        $quota = [
            'moox_news' => [
                0 => 'getAllowedQuotaEventNote',
                1 => 'getUsedEventNote'
            ],
            'moox_news_seminar_esb' => [
                0 => 'getAllowedQuotaEventNote',
                1 => 'getUsedEventNote'
            ],
            'moox_news_leisure_esb' => [
                0 => 'getAllowedQuotaEventNote',
                1 => 'getUsedEventNote'
            ],
            'moox_news_event_esb' => [
                0 => 'getAllowedQuotaEventNote',
                1 => 'getUsedEventNote'
            ],
            'moox_marketplace' => [
                0 => 'getAllowedQuotaMarketplace',
                1 => 'getUsedMarketplace'
            ]
        ];
        $getMethodMax = $quota[$newEntryType][0];
        $getMethodUsed = $quota[$newEntryType][1];
        $memberShip = $feUser->getAboMembership()->current();

        if (method_exists($memberShip, $getMethodMax) && method_exists($memberShip, $getMethodUsed)) {
            if ($memberShip->$getMethodMax() === -1 || $memberShip->$getMethodUsed() < $memberShip->$getMethodMax()) {
                return false;
            }
        }

        return true;
    }
}
