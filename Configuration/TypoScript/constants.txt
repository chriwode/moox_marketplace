plugin.tx_mooxmarketplace {
	view {
		# cat=plugin.tx_mooxmarketplace/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:moox_marketplace/Resources/Private/Templates/
		# cat=plugin.tx_mooxmarketplace/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:moox_marketplace/Resources/Private/Partials/
		# cat=plugin.tx_mooxmarketplace/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:moox_marketplace/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_mooxmarketplace/storage/01/; type=int+; label=Default storage PID
		storagePid =
	}
	settings {
		cssfiles {
			# cat=plugin.tx_mooxmarketplace/file/01; type=string; label=[Plugin 1] Path to local css file (leave blank for default css)
			pi1 =
			# cat=plugin.tx_mooxmarketplace/file/02; type=string; label=[Plugin 2] Path to local css file (leave blank for default css)
			pi2 =		
		}				
	}
}
module.tx_mooxmarketplace {
	persistence {
		# cat=plugin.tx_mooxmarketplace/storage/01/; type=int+; label=Default storage PID
		storagePid =
	}
}