<?php
// Set language source file
$ll = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang.xlf:';

// Get the extensions's configuration
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace']);

// hide sys category default fields for custom sys category variant 
$hideFields = "single_pid,preview_pid,shortcut,news_types,mailer_forcesinglemail,mailer_template,mailer_subject";

/**
 * Add extra fields to the sys_category record
 */
$newSysCategoryColumns = array(
	'pid' => array(
		'label' => 'pid',
		'config' => array(
			'type' => 'passthrough'
		)
	),
	'sorting' => array(
		'label' => 'sorting',
		'config' => array(
			'type' => 'passthrough'
		)
	),
	'crdate' => array(
		'label' => 'crdate',
		'config' => array(
			'type' => 'passthrough',
		)
	),
	'tstamp' => array(
		'label' => 'tstamp',
		'config' => array(
			'type' => 'passthrough',
		)
	),
	'variant' => array(
		'exclude' => 1,
		'label' => $ll.'category.variant',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array($ll.'category.variant.default', 'default'),
				array($ll.'category.variant.moox_marketplace', 'moox_marketplace'),					
			),
			'size' => 1,
			'maxitems' => 1,
		),			
	),
	'fe_group' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.fe_group',
		'config' => array(
			'type' => 'select',
			'size' => 6,
			'maxitems' => 99,
			'items' => array(
				array(
					'LLL:EXT:lang/locallang_general.xlf:LGL.hide_at_login',
					-1,
				),
				array(
					'LLL:EXT:lang/locallang_general.xlf:LGL.any_login',
					-2,
				),
				array(
					'LLL:EXT:lang/locallang_general.xlf:LGL.usergroups',
					'--div--',
				),
			),
			'exclusiveKeys' => '-1,-2',
			'foreign_table' => 'fe_groups',
			'foreign_table_where' => 'ORDER BY fe_groups.title',
		),
	),
	'images' => array(
		'exclude' => 1,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll . 'category.image',
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
			'images',
			array(
				'appearance' => array(
					'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
				),
				'foreign_match_fields' => array(
					'fieldname' => 'images',
					'tablenames' => 'sys_category',
					'table_local' => 'sys_file',
				),
			),
			$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
		)
	),	
	'classified_variants' => array(
		'exclude' => 1,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll . 'category.classified_variants',
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'config' => array(
			'type' => 'select',			
			'size' => 6,
			'maxitems' => 999,
			'minitems' => 0,
			'allowNonIdValues' => 1,						
			'default' => '',
			'itemsProcFunc' => 'DCNGmbH\MooxMarketplace\Hooks\TcaFormHelper->classifiedVariants',
		)
	),	
);

// Add new TCA fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $newSysCategoryColumns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', '--div--;LLL:EXT:cms/locallang_tca.xls:pages.tabs.options, images', '', 'before:description');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', 'variant', '', 'before:title');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', 'fe_group,classified_variants');

// hide sys category default fields defined in hide fields string 
if($hideFields!=""){
	$hideFields = explode(",",$hideFields);
	foreach($hideFields AS $hideField){
		if(isset($GLOBALS['TCA']['sys_category']['columns'][$hideField]) && $GLOBALS['TCA']['sys_category']['columns'][$hideField]['displayCond']==""){
			$GLOBALS['TCA']['sys_category']['columns'][$hideField]['displayCond'] = 'FIELD:variant:!=:moox_marketplace';
		}
	}
}

// add variant field to update fields

if(isset($GLOBALS['TCA']['sys_category']['ctrl']['requestUpdate']) && $GLOBALS['TCA']['sys_category']['ctrl']['requestUpdate']!=""){
	$GLOBALS['TCA']['sys_category']['ctrl']['requestUpdate'] = $GLOBALS['TCA']['sys_category']['ctrl']['requestUpdate'].',variant';
} else {
	$GLOBALS['TCA']['sys_category']['ctrl']['requestUpdate'] = 'variant';
}

// Add fe_group as enable field
$GLOBALS['TCA']['sys_category']['ctrl']['enablecolumns']['fe_group'] = 'fe_group';
$GLOBALS['TCA']['sys_category']['ctrl']['columns']['items']['config'] = array(
	'type' => 'passthrough'
);
$GLOBALS['TCA']['sys_category']['types'][1]['showitem'] = str_replace("--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.items,items,","",$GLOBALS['TCA']['sys_category']['types'][1]['showitem']);

// restrict category selection if set in extension configuration
if($extConf['categoryRestriction']=="current_pid"){
	$GLOBALS['TCA']['sys_category']['columns']['parent']['config']['foreign_table_where'] = ' AND pid=###CURRENT_PID###'.$GLOBALS['TCA']['sys_category']['columns']['parent']['config']['foreign_table_where'];
}
