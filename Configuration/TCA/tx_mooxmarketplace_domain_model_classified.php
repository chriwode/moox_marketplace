<?php
// Set language source file
$ll = 'LLL:EXT:moox_marketplace/Resources/Private/Language/locallang.xlf:';

// Get the extensions's configuration
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace']);

// tca configuration array
$tx_mooxmarketplace_domain_model_classified = array(
	'ctrl' => array(
		'title'	=> 'Kleinanzeige',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),		
		'searchFields' => 'title,description',		
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('moox_marketplace').'Resources/Public/Icons/tx_mooxmarketplace_domain_model_classified.gif',
		'requestUpdate' => 'variant',
		'hideTable' => FALSE,
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, categories, images, files',
	),
	'types' => array(
		'1' => array('showitem' => 
			//'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,'. 
			'variant, title, description,'.			
			'--div--;Kategorien, categories,'.
			'--div--;Medien, images, files,'.
			'--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,hidden, starttime, endtime'			
		),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mooxmarketplace_domain_model_classified',
				'foreign_table_where' => 'AND tx_mooxmarketplace_domain_model_classified.pid=###CURRENT_PID### AND tx_mooxmarketplace_domain_model_classified.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'default' => 1,
				'plugins' => array(
					"mooxmarketplace" => array(
						"add","edit"
					),
				),				
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"list","detail"
					),					
				),
				'sortable' => 1,
			),
		),
		'cruser_id' => array(
			'label' => 'cruser_id',
			'config' => array(
				'type' => 'passthrough'
			),			
		),
		'uid' => array(
			'label' => 'uid',
			'config' => array(
				'type' => 'passthrough'
			),			
		),
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			),			
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"list","detail"
					),
				),
				'sortable' => 1,
			),
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"list","detail"
					),
				),
				'sortable' => 1,
			),
		),
		'fe_user' => array(			
			'exclude' => 0,
			'label' => $ll.'form.fe_user',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'default' => '',
				'foreign_table' => 'fe_users',
				'foreign_table_where' => 'ORDER BY fe_users.last_name',
        		'size' => 1,				
				'maxitems' => 1,
				'minitems' => 0,
				'multiple' => 0,        		
				'items' => array(
					array('Keine Auswahl', ''),
				),								
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',				
			),
		),
		'fe_group' => array(			
			'exclude' => 0,
			'label' => $ll.'form.fe_group',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'default' => '',
				'foreign_table' => 'fe_groups',
				'foreign_table_where' => 'ORDER BY fe_groups.title',
        		'size' => 1,				
				'maxitems' => 1,
				'minitems' => 0,
				'multiple' => 0,        		
				'items' => array(
					array('Keine Auswahl', ''),
				),								
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',				
			),
		),
		'categories' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => $ll.'form.categories',
			'config' => array(
				/*
				'type' => 'user',
				'userFunc' => 'DCNGmbH\MooxMarketplace\Hooks\TcaFormHelper->treeSelector',							
                */
				'type' => 'select',
				'renderType' => 'selectTree',
				'renderMode' => 'tree',
				'treeConfig' => array(
                    'expandAll' => true,
                    'parentField' => 'parent',
                    'appearance' => array(
                        'showHeader' => TRUE,
                        'width' => 400
                    ),
                ),				
				'MM' => 'sys_category_record_mm',
				'MM_match_fields' => array(
					'fieldname' => 'categories',
					'tablenames' => 'tx_mooxmarketplace_domain_model_classified',
				),
				'MM_opposite_field' => 'items',
				'foreign_table' => 'sys_category',
				'foreign_table_where' => ' AND variant="moox_marketplace" AND (sys_category.sys_language_uid = 0 OR sys_category.l10n_parent = 0) ORDER BY sys_category.sorting',		
				'minitems' => ($extConf['minCategoryCount']>0)?$extConf['minCategoryCount']:0,
				'maxitems' => ($extConf['maxCategoryCount']>0)?$extConf['maxCategoryCount']:9999,
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"add","edit","list","detail"
					),
				),				
			),
		),
		'variant' => array(
			'exclude' => 1,
			'label' => $ll.'form.variant',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,
				'items' => array(
					array($ll.'form.variant.moox_marketplace', 'moox_marketplace'),					
				),
				'size' => 1,
				'maxitems' => 1,
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"add","list","detail"
					),
				),
				'sortable' => 1,
			),		
		),
		'title' => array(
			'exclude' => 0,
			'label' => $ll.'form.title',
			'config' => array(
				'type' => 'input',
				'size' => 40,
				'eval' => 'required,trim'
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',				
				'plugins' => array(
					"mooxmarketplace"
				),
				'sortable' => 1,
			),			
		),		
		'description' => array(
			'exclude' => 0,
			'label' => $ll.'form.description',			
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 10,
				'eval' => 'trim'
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"add","edit","list","detail"
					),
				),
			),
		),
		'images' => array(
			'exclude' => 0,
			'label' => $ll.'form.images',			
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'images',
				array(
					'reference' => 'image',
					'maxitems' => 7,
					'maxfilesize' => 20480,
					'accepts' => 'jpg,jpeg,png,gif',
					'uploadfolder' => ($extConf['uploadFolder']!="")?$extConf['uploadFolder']:'uploads/tx_mooxmarketplace',
					'appearance' => array(
						'headerThumbnail' => array(
							'width' => '100',
							'height' => '100',
						),
						'createNewRelationLinkTitle' => 'Bild hinzufügen',
					),
					// custom configuration for displaying fields in the overlay/reference table
					// to use the imageoverlayPalette instead of the basicoverlayPalette
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						)
					),
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"add","edit","list","detail"
					),
				),
			),
		),
		'files' => array(
			'exclude' => 0,
			'label' => $ll.'form.files',		
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'files',
				array(
					'reference' => 'file',
					'maxitems' => 3,
					'maxfilesize' => 20480,
					'accepts' => 'pdf',
					'uploadfolder' => ($extConf['uploadFolder']!="")?$extConf['uploadFolder']:'uploads/tx_mooxmarketplace',
					'appearance' => array(
						'headerThumbnail' => array(
							'width' => '100',
							'height' => '100',
						),
						'createNewRelationLinkTitle' => 'Datei hinzufügen',
					),
					// custom configuration for displaying fields in the overlay/reference table
					// to use the imageoverlayPalette instead of the basicoverlayPalette
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						)
					),
				),
				'pdf'
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',
				'plugins' => array(
					"mooxmarketplace" => array(
						"add","edit","detail"
					),
				),
			),
		),
		'paid' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => $ll.'form.paid',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,				
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',				
			),
		),
		'paid_by' => Array (		
			'exclude' => 1,		
			'label' => $ll.'form.paid_by',
			'config' => array(
				'type' => 'select',
				'allowNonIdValues' => 1,				
				'size' => 1,
				'maxitems' => 1,
				'items' => array(
					array("No Payment", '0'),					
				),
				'itemsProcFunc' => 'DCNGmbH\MooxMarketplace\Hooks\TcaFormHelper->paymentMethods',
			),
			// special moox configuration		
			'moox' => array(
				'extkey' => 'moox_marketplace',				
			),
		),
		'paid_info' => array(
			'label' => 'paid info',
			'config' => array(
				'type' => 'passthrough'
			),			
		),		
	),
);

// check if payment module is installed
if(!\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('moox_payment')){
	unset($tx_mooxmarketplace_domain_model_classified['columns']['paid']);
	unset($tx_mooxmarketplace_domain_model_classified['columns']['paid_by']);	
	unset($tx_mooxmarketplace_domain_model_classified['columns']['paid_info']);
} else {
	$tx_mooxmarketplace_domain_model_classified['types'][1]['showitem'] .= ",--div--;Bezahlung, paid, paid_by";
}

// category restriction based on settings in extension manager
if ($extConf['categoryRestriction']){
	switch ($extConf['categoryRestriction']) {
		case 'current_pid':
			$categoryRestriction = ' AND sys_category.pid=###CURRENT_PID### ';
			break;
		case 'storage_pid':
			$categoryRestriction = ' AND sys_category.pid=###STORAGE_PID### ';
			break;
		case 'siteroot':
			$categoryRestriction = ' AND sys_category.pid IN (###SITEROOT###) ';
			break;
		case 'page_tsconfig':
			$categoryRestriction = ' AND sys_category.pid IN (###PAGE_TSCONFIG_IDLIST###) ';
			break;
		default:
			$categoryRestriction = '';
	}

	// prepend category restriction at the beginning of foreign_table_where
	if (!empty ($categoryRestriction)) {
		$tx_mooxmarketplace_domain_model_classified['columns']['categories']['config']['foreign_table_where'] = $categoryRestriction.
		$tx_mooxmarketplace_domain_model_classified['columns']['categories']['config']['foreign_table_where'];
	}
}

// return tca configuration
return $tx_mooxmarketplace_domain_model_classified;
?>