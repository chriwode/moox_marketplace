<?php
defined('TYPO3_MODE') or die();

// register frontend plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'DCNGmbH.' . $_EXTKEY,
	'Pi1',
	array(
		'Pi1' => 'list,detail,add,edit,delete,visibility,error',
	),
	// non-cacheable actions
	array(
		'Pi1' => 'list,detail,add,edit,delete,visibility,error',
	)
);

// register frontend plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'DCNGmbH.' . $_EXTKEY,
	'Pi2',
	array(
		'Pi2' => 'list,detail,error',
	),
	// non-cacheable actions
	array(
		'Pi2' => 'list,detail,error',
	)
);

// hook to process items aber saving
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$packageKey] = 'DCNGmbH\MooxMarketplace\Hooks\Tcemain';

// Register cache frontend for proxy class generation
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['moox_marketplace'] = [
	'frontend' => \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend::class,
	'backend' => \TYPO3\CMS\Core\Cache\Backend\FileBackend::class,
	'groups' => [
		'all',
		'system',
	],
	'options' => [
		'defaultLifetime' => 0,
	]
];

// Register autoloader
\DCNGmbH\MooxMarketplace\Utility\ClassLoader::registerAutoloader();

?>