<?php
$extensionClassesPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('moox_marketplace') . 'Classes/';
require_once($extensionClassesPath.'Cache/ClassCacheBuilder.php');

$default = array(
	'DCNGmbH\MooxMarketplace\Cache\ClassCacheBuilder' => $extensionClassesPath . 'Cache/ClassCacheBuilder.php',
	'DCNGmbH\MooxMarketplace\Domain\Model\Classified' => $extensionClassesPath . 'Domain/Model/Classified.php',
);

/** @var DCNGmbH\MooxMarketplace\Cache\ClassCacheBuilder $classCacheBuilder */
$classCacheBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('DCNGmbH\MooxMarketplace\Cache\ClassCacheBuilder');
$mergedClasses = array_merge($default, $classCacheBuilder->build());
return $mergedClasses;
?>
